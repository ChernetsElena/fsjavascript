# Проект Подсчет микроэлементов
## MVP 1
- авторизация
- главная страница
- редактирование элемента списка

### 1. Установка
```shell
npm install
```
### 2. Запуск
```shell
npm start
```

[figma](https://www.figma.com/file/46XqUOTgxEJYtZkBJGcVrK/Untitled)
