const router = require("express").Router();
const food = require('./food-products.json');
const norm = require('./norm.json');

const users = [
    {
        id: 0, 
        userName: "Anna1234", 
        password: "81dc9bdb52d04dc20036dbd8313ed055",
        email : "ann1234@mail.ru"
    }, 
    {
        id : 1,
        userName: "Andrew", 
        password: "d8578edf8458ce06fbc5bb76a58c5ca4",
        email : "andrew1234@mail.ru"
    },
]

const nutrientsDay = [
    { text: 'Na', name: 'Na', percent: 0 },
    { text: 'K', name: 'K', percent: 0 },
    { text: 'Ca', name: 'Ca', percent: 0 },
    { text: 'Mg', name: 'Mg', percent: 0 },
    { text: 'C', name: 'C', percent: 0 },
    { text: 'P', name: 'P', percent: 0 },
    { text: 'Fe', name: 'Fe', percent: 0 },
    { text: 'A', name: 'A', percent: 0 },
    { text: 'b-car', name: 'bcar', percent: 0 },
    { text: 'I', name: 'I', percent: 0 },
    { text: 'Калории', name: 'calories', percent: 0 },
    { text: 'Белки', name: 'proteins', percent: 0 },
    { text: 'Жиры', name: 'fats', percent: 0 },
    { text: 'Углеводы', name: 'carbohydrates', percent: 0 },
    { text: 'Вода', name: 'water', percent: 0 },
    { text: 'Клетчатка', name: 'cellulose', percent: 0 },
];

const breakfast = [
    { text: 'Na', name: 'Na', percent: 0 },
    { text: 'K', name: 'K', percent: 0 },
    { text: 'Ca', name: 'Ca', percent: 0 },
    { text: 'Mg', name: 'Mg', percent: 0 },
    { text: 'C', name: 'C', percent: 0 },
    { text: 'P', name: 'P', percent: 0 },
    { text: 'Fe', name: 'Fe', percent: 0 },
    { text: 'A', name: 'A', percent: 0 },
    { text: 'b-car', name: 'bcar', percent: 0 },
    { text: 'I', name: 'I', percent: 0 },
    { text: 'Калории', name: 'calories', percent: 0 },
    { text: 'Белки', name: 'proteins', percent: 0 },
    { text: 'Жиры', name: 'fats', percent: 0 },
    { text: 'Углеводы', name: 'carbohydrates', percent: 0 },
    { text: 'Вода', name: 'water', percent: 0 },
    { text: 'Клетчатка', name: 'cellulose', percent: 0 },
];

const lunch = [
    { text: 'Na', name: 'Na', percent: 0 },
    { text: 'K', name: 'K', percent: 0 },
    { text: 'Ca', name: 'Ca', percent: 0 },
    { text: 'Mg', name: 'Mg', percent: 0 },
    { text: 'C', name: 'C', percent: 0 },
    { text: 'P', name: 'P', percent: 0 },
    { text: 'Fe', name: 'Fe', percent: 0 },
    { text: 'A', name: 'A', percent: 0 },
    { text: 'b-car', name: 'bcar', percent: 0 },
    { text: 'I', name: 'I', percent: 0 },
    { text: 'Калории', name: 'calories', percent: 0 },
    { text: 'Белки', name: 'proteins', percent: 0 },
    { text: 'Жиры', name: 'fats', percent: 0 },
    { text: 'Углеводы', name: 'carbohydrates', percent: 0 },
    { text: 'Вода', name: 'water', percent: 0 },
    { text: 'Клетчатка', name: 'cellulose', percent: 0 },
];

const dinner = [
    { text: 'Na', name: 'Na', percent: 0 },
    { text: 'K', name: 'K', percent: 0 },
    { text: 'Ca', name: 'Ca', percent: 0 },
    { text: 'Mg', name: 'Mg', percent: 0 },
    { text: 'C', name: 'C', percent: 0 },
    { text: 'P', name: 'P', percent: 0 },
    { text: 'Fe', name: 'Fe', percent: 0 },
    { text: 'A', name: 'A', percent: 0 },
    { text: 'b-car', name: 'bcar', percent: 0 },
    { text: 'I', name: 'I', percent: 0 },
    { text: 'Калории', name: 'calories', percent: 0 },
    { text: 'Белки', name: 'proteins', percent: 0 },
    { text: 'Жиры', name: 'fats', percent: 0 },
    { text: 'Углеводы', name: 'carbohydrates', percent: 0 },
    { text: 'Вода', name: 'water', percent: 0 },
    { text: 'Клетчатка', name: 'cellulose', percent: 0 },
];

router.post('/login', (req, res) => {
    if (users.some((user) => (user.userName == req.body.userName))) {
        userLogin = users.filter((user) => (user.userName == req.body.userName))
        if (userLogin[0].password == req.body.password) {
            res.send(require("./mocks/login/success") );
        }
        else {
            res.status(400).send(require('./mocks/login/not-success'));
        }
    }
    else {
        res.status(400).send(require('./mocks/login/not-success'));
    }
})

router.post('/recovery/email', (req, res) => {
    if (users.some((user) => (user.email == req.body.email))) {
        userRecovery = users.filter((user) => (user.email == req.body.email))
        res.send( require("./mocks/recovery/email/success") );
    } else {
        res.status(400).send( require("./mocks/recovery/email/not-success") );
    }
})

router.post('/recovery/code', (req, res) => {
    const code = '1234';
    if (req.body.code == code) {
        res.send( require("./mocks/recovery/code/success") );
    } else {
        res.status(400).send(require('./mocks/recovery/code/not-success'));
    }
})

router.post('/recovery/password', (req, res) => {
    for ( let user = 0; user < users.length; user++ ) {
        if (users[user].email == userRecovery[0].email) {
            users[user].password = req.body.password;
        }
    }
    res.send( require("./mocks/recovery/password/success") );
})


router.post('/register/data', (req, res) => {
    if (users.some((user) => (user.userName == req.body.userName))) {
        res.status(400).send(require('./mocks/register/data/not-success-username'));
    } else if (users.some((user) => (user.email == req.body.email))) {
        res.status(400).send(require('./mocks/register/data/not-success-email'));
    } else {
        users.push({"id" : users.length, "userName": req.body.userName, "password": req.body.password, "email": req.body.email});
        res.send( require("./mocks/register/data/success") );
    }
})

router.post('/register/code', (req, res) => {
    const code = '1234'
    if (req.body.code == code) {
        res.send( require("./mocks/register/code/success") );
    } else {
        res.status(400).send(require('./mocks/register/code/not-success'));
    }
})

router.post('/main/addfood', (req, res) => {
    if (req.body.complete === false) {
        if (isNaN(Number(req.body.foodWeight))) {
            weight = 0;
        } else {
            weight = req.body.foodWeight;
        }
        if (req.body.foodName in food) {
            const data = {};
            const answer = {day: {}, breakfast: {}, lunch: {}, dinner: {}}
            for (nutrient in food[req.body.foodName]){
                const [quantity, measure] = Object.entries(food[req.body.foodName][nutrient])
                const [name, number] = quantity;
                const [quantityNorm, measureNorm] = Object.entries(norm[nutrient]);
                const [nameNorm, numberNorm] = quantityNorm;
                data[nutrient] = Math.floor((number * weight / numberNorm) * 10) / 10;
                nutrientsDay.map(item => {
                    if (item.name === nutrient){
                        item.percent += data[nutrient];
                        answer.day[nutrient] = Math.floor(item.percent * 10) / 10;
                    }
                    return item, answer;
                });
                breakfast.map(item => {
                    if (item.name === nutrient){
                        answer.breakfast[nutrient] = Math.floor(item.percent * 10) / 10;
                    }
                    return answer;
                });
                lunch.map(item => {
                    if (item.name === nutrient){
                        answer.lunch[nutrient] = Math.floor(item.percent * 10) / 10;
                    }
                    return answer;
                });
                dinner.map(item => {
                    if (item.name === nutrient){
                        answer.dinner[nutrient] = Math.floor(item.percent * 10) / 10;
                    };
                    return answer;
                });
                if (req.body.eatTime === 'Завтрак') {
                    breakfast.map(item => {
                        if (item.name === nutrient){
                            item.percent += data[nutrient];
                            answer.breakfast[nutrient] = Math.floor(item.percent * 10) / 10;
                        }
                        return item, answer;
                    });
                };
                if (req.body.eatTime === 'Обед') {
                    lunch.map(item => {
                        if (item.name === nutrient){
                            item.percent += data[nutrient];
                            answer.lunch[nutrient] = Math.floor(item.percent * 10) / 10;
                        }
                        return item, answer;
                    });
                };
                if (req.body.eatTime === 'Ужин') {
                    dinner.map(item => {
                        if (item.name === nutrient){
                            item.percent += data[nutrient];
                            answer.dinner[nutrient] = Math.floor(item.percent * 10) / 10;
                        };
                        return item, answer;
                    });
                }
            }
            res.send(answer);
        } else {
            res.status(400).send(require('./mocks/main/add-food/not-success'));
        }
    } else {
        const answer = {day: {}, breakfast: {}, lunch: {}, dinner: {}};
        nutrientsDay.map(item => {
            item.percent = 0;
            answer.day[item.name] = 0;
            return item, answer;
        });
        breakfast.map(item => {
            item.percent = 0;
            answer.breakfast[item.name] = 0;
            return item, answer;
        });
        lunch.map(item => {
            item.percent = 0;
            answer.lunch[item.name] = 0;
            return item, answer;
        });
        dinner.map(item => {
            item.percent = 0;
            answer.dinner[item.name] = 0;
            return item, answer;
        });
        res.send(answer);
    }
})

router.post('/main/showfoodnutrients', (req, res) => {
    if (req.body.foodName in food) {
        const data = {};
        for (nutrient in food[req.body.foodName]){
            const [quantity, measure] = Object.entries(food[req.body.foodName][nutrient])
            const [name, number] = quantity;
            const [quantityNorm, measureNorm] = Object.entries(norm[nutrient]);
            const [nameNorm, numberNorm] = quantityNorm;
            data[nutrient] = Math.floor((number * 100 / numberNorm) * 10) / 10;
        }
        res.send(data);
    } else {
        res.status(400).send(require('./mocks/main/show-food-nutrients/not-success'));
    }
})

module.exports = router;
