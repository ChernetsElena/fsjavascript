﻿const {navigations, config} = require('../ijl.config')
module.exports = {
    getNavigations: () => {
        return {
            ...navigations
        }
    },
    getNavigationsValue: key => navigations[key],
    getConfigValue: (key) => config[key]
}