﻿import { getNavigations } from '@ijl/cli';

const navigations = getNavigations('fsjavascript');

export const baseUrl = navigations['fsjavascript'];

export const URLs = {
    root: {
        url: navigations['fsjavascript'],
    },

    login: {
        url: navigations['link.fsjavascript.login'],
    },

    register: {
        url: navigations['link.fsjavascript.register'],
    },

    recovery: {
        url: navigations['link.fsjavascript.recovery'],
    },

    main: {
        url: navigations['link.fsjavascript.main'],
    },
};
