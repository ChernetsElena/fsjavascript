import * as types from '../action-types';
import axios from 'axios';
import { getConfigValue } from '@ijl/cli';
import md5 from 'md5';

const submitActionCreator = () => ({
    type: types.RECOVERY_INPUT_PASSWORD.SUBMIT,
});
const successActionCreator = (data) => ({
    type: types.RECOVERY_INPUT_PASSWORD.SUCCESS,
    data,
});
const failureActionCreator = (error) => ({
    type: types.RECOVERY_INPUT_PASSWORD.FAILURE,
    error,
});

export const setValidationError = (error) => ({
    type: types.RECOVERY_INPUT_PASSWORD.SET_VALIDATION_ERROR,
    payload: error,
});

export const formPasswordChange = (value: string) => ({
    type: types.RECOVERY_INPUT_PASSWORD.FORM_PASSWORD_CHANGE,
    value,
});

export const formPasswordRepeatChange = (value: string) => ({
    type: types.RECOVERY_INPUT_PASSWORD.FORM_PASSWORD_REPEAT_CHANGE,
    value,
});

export const submitRecoveryFormPassword = ({ password }) => async (
    dispatch
) => {
    const rawData = { password: md5(password) };
    const baseApiUrl = getConfigValue('fsjavascript.api');

    dispatch(submitActionCreator());
    try {
        const response = await axios.post(
            `${baseApiUrl}/recovery/password`,
            rawData
        );
        dispatch(successActionCreator(response.data));
    } catch (error) {
        dispatch(
            failureActionCreator(
                error?.response?.data?.error || 'Неизвестная ошибка'
            )
        );
    }
};
