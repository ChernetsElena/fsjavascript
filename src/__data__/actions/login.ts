import * as types from '../action-types';
import md5 from 'md5';
import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

const submitActionCreator = () => ({ type: types.LOGIN.SUBMIT });
const successActionCreator = (data) => ({ type: types.LOGIN.SUCCESS, data });
const failureActionCreator = (error) => ({
    type: types.LOGIN.FAILURE,
    error,
});

export const setValidationError = (error) => ({
    type: types.LOGIN.SET_VALIDATION_ERROR,
    payload: error,
});

export const formUserNameChange = (value: string) => ({
    type: types.LOGIN.FORM_USERNAME_CHANGE,
    value,
});

export const formPasswordChange = (value: string) => ({
    type: types.LOGIN.FORM_PASSWORD_CHANGE,
    value,
});

export const submitLoginForm = ({ userName, password }) => async (dispatch) => {
    const rawData = { userName, password: md5(password) };
    const baseApiUrl = getConfigValue('fsjavascript.api');

    dispatch(submitActionCreator());
    try {
        const response = await axios.post(`${baseApiUrl}/login`, rawData);
        dispatch(successActionCreator(response.data));
    } catch (error) {
        dispatch(
            failureActionCreator(
                error?.response?.data?.error || 'Неизвестная ошибка'
            )
        );
    }
};
