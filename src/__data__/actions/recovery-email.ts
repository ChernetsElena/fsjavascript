import * as types from '../action-types';
import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

const submitActionCreator = () => ({ type: types.RECOVERY_INPUT_EMAIL.SUBMIT });
const successActionCreator = (data) => ({
    type: types.RECOVERY_INPUT_EMAIL.SUCCESS,
    data,
});
const failureActionCreator = (error) => ({
    type: types.RECOVERY_INPUT_EMAIL.FAILURE,
    error,
});

export const setValidationError = (error) => ({
    type: types.RECOVERY_INPUT_EMAIL.SET_VALIDATION_ERROR,
    payload: error,
});

export const formEmailChange = (value: string) => ({
    type: types.RECOVERY_INPUT_EMAIL.FORM_EMAIL_CHANGE,
    value,
});

export const submitRecoveryFormEmail = ({ email }) => async (dispatch) => {
    const rawData = { email };
    const baseApiUrl = getConfigValue('fsjavascript.api');

    dispatch(submitActionCreator());
    try {
        const response = await axios.post(
            `${baseApiUrl}/recovery/email`,
            rawData
        );
        dispatch(successActionCreator(response.data));
    } catch (error) {
        dispatch(
            failureActionCreator(
                error?.response?.data?.error || 'Неизвестная ошибка'
            )
        );
    }
};
