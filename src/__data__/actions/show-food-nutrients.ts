﻿import * as types from '../action-types';
import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

const submitActionCreator = () => ({ type: types.FOOD_NUTRIENTS.SUBMIT });
const successActionCreator = (data) => ({
    type: types.FOOD_NUTRIENTS.SUCCESS,
    data,
});
const failureActionCreator = (error) => ({
    type: types.FOOD_NUTRIENTS.FAILURE,
    error,
});

export const formFoodNameChange = (value: string) => ({
    type: types.FOOD_NUTRIENTS.FORM_FOOD_NAME_CHANGE,
    value,
});

export const submitFoodNutrients = (foodName) => async (dispatch) => {
    const rawData = { foodName };
    const baseApiUrl = getConfigValue('fsjavascript.api');

    dispatch(submitActionCreator());
    try {
        const response = await axios.post(
            `${baseApiUrl}/main/showfoodnutrients`,
            rawData
        );
        dispatch(successActionCreator(response.data));
    } catch (error) {
        dispatch(
            failureActionCreator(
                error?.response?.data?.error || 'Неизвестная ошибка'
            )
        );
    }
};
