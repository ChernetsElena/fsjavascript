import * as types from '../action-types';
import md5 from 'md5';
import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

const submitActionCreator = () => ({ type: types.REGISTER_INPUT_DATA.SUBMIT });
const successActionCreator = (data) => ({
    type: types.REGISTER_INPUT_DATA.SUCCESS,
    data,
});
const failureActionCreator = (error) => ({
    type: types.REGISTER_INPUT_DATA.FAILURE,
    error,
});

export const setValidationError = (error) => ({
    type: types.REGISTER_INPUT_DATA.SET_VALIDATION_ERROR,
    payload: error,
});

export const formUserNameChange = (value: string) => ({
    type: types.REGISTER_INPUT_DATA.FORM_USERNAME_CHANGE,
    value,
});

export const formPasswordChange = (value: string) => ({
    type: types.REGISTER_INPUT_DATA.FORM_PASSWORD_CHANGE,
    value,
});

export const formPasswordRepeatChange = (value: string) => ({
    type: types.REGISTER_INPUT_DATA.FORM_PASSWORD_REPEAT_CHANGE,
    value,
});

export const formEmailChange = (value: string) => ({
    type: types.REGISTER_INPUT_DATA.FORM_EMAIL_CHANGE,
    value,
});

export const submitRegisterFormData = ({ userName, password, email }) => async (
    dispatch
) => {
    const rawData = { userName, password: md5(password), email };
    const baseApiUrl = getConfigValue('fsjavascript.api');

    dispatch(submitActionCreator());
    try {
        const response = await axios.post(
            `${baseApiUrl}/register/data`,
            rawData
        );
        dispatch(successActionCreator(response.data));
    } catch (error) {
        dispatch(
            failureActionCreator(
                error?.response?.data?.error || 'Неизвестная ошибка'
            )
        );
    }
};
