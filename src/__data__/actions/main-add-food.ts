﻿import * as types from '../action-types';
import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

const submitActionCreator = () => ({ type: types.MAIN.SUBMIT });
const successActionCreator = (data) => ({ type: types.MAIN.SUCCESS, data });
const failureActionCreator = (error) => ({
    type: types.MAIN.FAILURE,
    error,
});

export const setValidationError = (error) => ({
    type: types.MAIN.SET_VALIDATION_ERROR,
    payload: error,
});

export const formFoodNameChange = (value: string) => ({
    type: types.MAIN.FORM_FOOD_NAME_CHANGE,
    value,
});

export const formFoodWeightChange = (value: string) => ({
    type: types.MAIN.FORM_FOOD_WEIGHT_CHANGE,
    value,
});

export const formEatTimeChange = (value: string) => ({
    type: types.MAIN.FORM_EAT_TIME_CHANGE,
    value,
});

export const submitMainFoodForm = ({
    foodName,
    foodWeight,
    eatTime,
    complete,
}) => async (dispatch) => {
    const rawData = { foodName, foodWeight, eatTime, complete };
    const baseApiUrl = getConfigValue('fsjavascript.api');

    dispatch(submitActionCreator());
    try {
        const response = await axios.post(
            `${baseApiUrl}/main/addfood`,
            rawData
        );
        dispatch(successActionCreator(response.data));
    } catch (error) {
        dispatch(
            failureActionCreator(
                error?.response?.data?.error || 'Неизвестная ошибка'
            )
        );
    }
};
