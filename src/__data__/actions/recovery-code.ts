import * as types from '../action-types';
import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

const submitActionCreator = () => ({ type: types.RECOVERY_INPUT_CODE.SUBMIT });
const successActionCreator = (data) => ({
    type: types.RECOVERY_INPUT_CODE.SUCCESS,
    data,
});
const failureActionCreator = (error) => ({
    type: types.RECOVERY_INPUT_CODE.FAILURE,
    error,
});

export const setValidationError = (error) => ({
    type: types.RECOVERY_INPUT_CODE.SET_VALIDATION_ERROR,
    payload: error,
});

export const formCodeChange = (value: string) => ({
    type: types.RECOVERY_INPUT_CODE.FORM_CODE_CHANGE,
    value,
});

export const submitRecoveryFormCode = ({ code }) => async (dispatch) => {
    const rawData = { code };
    const baseApiUrl = getConfigValue('fsjavascript.api');

    dispatch(submitActionCreator());
    try {
        const response = await axios.post(
            `${baseApiUrl}/recovery/code`,
            rawData
        );
        dispatch(successActionCreator(response.data));
    } catch (error) {
        dispatch(
            failureActionCreator(
                error?.response?.data?.error || 'Неизвестная ошибка'
            )
        );
    }
};
