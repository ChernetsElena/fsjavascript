import { combineReducers } from 'redux';
import loginReducer, { LoginState } from './login/login';
import recoveryEmailReducer, {
    RecoveryEmailState,
} from './recovery/input-email';
import recoveryCodeReducer, { RecoveryCodeState } from './recovery/input-code';
import recoveryPasswordReducer, {
    RecoveryPasswordState,
} from './recovery/input-password';
import registerCodeReducer, { RegisterCodeState } from './register/input-code';
import registerDataReducer, { RegisterDataState } from './register/input-data';
import mainReducer, { AddEatFormState } from './main/add-food';
import showFoodNutrientsReducer, {
    ShowEatFormState,
} from './main/show-food-nutrients';

export type AppStore = {
    register: {
        inputData: RegisterDataState;
        inputCode: RegisterCodeState;
    };
    recovery: {
        inputEmail: RecoveryEmailState;
        inputCode: RecoveryCodeState;
        inputPassword: RecoveryPasswordState;
    };
    login: LoginState;
    main: {
        addEat: AddEatFormState;
        showNutrients: ShowEatFormState;
    };
};

export default combineReducers<AppStore>({
    register: combineReducers({
        inputData: registerDataReducer,
        inputCode: registerCodeReducer,
    }),
    recovery: combineReducers({
        inputEmail: recoveryEmailReducer,
        inputCode: recoveryCodeReducer,
        inputPassword: recoveryPasswordReducer,
    }),
    login: loginReducer,
    main: combineReducers({
        addEat: mainReducer,
        showNutrients: showFoodNutrientsReducer,
    }),
});
