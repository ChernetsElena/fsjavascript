﻿import * as types from '../../action-types';

type AddEatForm = {
    foodName: string;
    foodWeight: string;
    eatTime: string;
};

type NutrientsR = {
    Na: { text: string; name: string; percent: number };
    Ca: { text: string; name: string; percent: number };
    K: { text: string; name: string; percent: number };
    Mg: { text: string; name: string; percent: number };
    P: { text: string; name: string; percent: number };
};

type NutrientsL = [
    Fe: { text: string; name: string; percent: number },
    C: { text: string; name: string; percent: number },
    A: { text: string; name: string; percent: number },
    bcar: { text: string; name: string; percent: number },
    I: { text: string; name: string; percent: number }
];

type Macronutrients = {
    calories: { text: string; name: string; percent: number };
    proteins: { text: string; name: string; percent: number };
    fats: { text: string; name: string; percent: number };
    carbohydrates: { text: string; name: string; percent: number };
    cellulose: { text: string; name: string; percent: number };
    water: { text: string; name: string; percent: number };
};

export type AddEatFormState = {
    loading: boolean;
    data: any;
    error: boolean | string;
    validationError: boolean | string;
    complete: boolean;
    form: AddEatForm;
    nutrientsLeft: NutrientsL;
    nutrientsRight: NutrientsR;
    macronutrients: Macronutrients;
};

const initialState = {
    loading: false,
    data: null,
    error: false,
    validationError: false,
    complete: false,
    form: {
        foodName: '',
        foodWeight: '',
        eatTime: '',
    },
    nutrientsLeft: {
        Fe: { text: 'Fe', name: 'Fe', percent: 0 },
        C: { text: 'C', name: 'C', percent: 0 },
        A: { text: 'A', name: 'A', percent: 0 },
        bcar: { text: 'bcar', name: 'bcar', percent: 0 },
        I: { text: 'I', name: 'I', percent: 0 },
    },
    nutrientsRight: {
        Na: { text: 'Na', name: 'Na', percent: 0 },
        Ca: { text: 'Ca', name: 'Ca', percent: 0 },
        K: { text: 'K', name: 'K', percent: 0 },
        Mg: { text: 'Mg', name: 'Mg', percent: 0 },
        P: { text: 'P', name: 'P', percent: 0 },
    },
    macronutrients: {
        calories: { text: 'Калории', name: 'calories', percent: 0 },
        proteins: { text: 'Белки', name: 'proteins', percent: 0 },
        fats: { text: 'Жиры', name: 'fats', percent: 0 },
        carbohydrates: { text: 'Углеводы', name: 'carbohydrates', percent: 0 },
        cellulose: { text: 'Клетчатка', name: 'cellulose', percent: 0 },
        water: { text: 'Вода', name: 'water', percent: 0 },
    },
};

const handleSubmit = (state, action) => ({
    ...state,
    loading: true,
    error: false,
    complete: { ...state.complete },
    form: { ...state.form },
    nutrientsLeft: { ...state.nutrientsLeft },
    nutrientsRight: { ...state.nutrientsRight },
    macronutrients: { ...state.macronutrients },
});

const handleSuccess = (state, action) => ({
    ...initialState,
    form: { ...initialState.form },
    complete: false,
    data: action.data,
    nutrientsLeft: {
        ...state.nutrientsLeft,
        Fe: {
            ...state.Fe,
            percent: action.data.Fe,
        },
        C: {
            ...state.C,
            percent: action.data.C,
        },
        A: {
            ...state.A,
            percent: action.data.A,
        },
        bcar: {
            ...state.bcar,
            percent: action.data.bcar,
        },
        I: {
            ...state.I,
            percent: action.data.I,
        },
    },

    nutrientsRight: {
        ...state.nutrientsRight,
        Na: {
            ...state.Na,
            percent: action.data.Na,
        },
        Ca: {
            ...state.Ca,
            percent: action.data.Ca,
        },
        K: {
            ...state.K,
            percent: action.data.K,
        },
        Mg: {
            ...state.Mg,
            percent: action.data.Mg,
        },
        P: {
            ...state.P,
            percent: action.data.P,
        },
    },

    macronutrients: {
        ...state.macronutrients,
        calories: {
            ...state.calories,
            percent: action.data.calories,
        },
        proteins: {
            ...state.proteins,
            percent: action.data.proteins,
        },
        fats: {
            ...state.fats,
            percent: action.data.fats,
        },
        carbohydrates: {
            ...state.carbohydrates,
            percent: action.data.carbohydrates,
        },
        cellulose: {
            ...state.cellulose,
            percent: action.data.celluloseP,
        },
        water: {
            ...state.water,
            percent: action.data.water,
        },
    },
});

const handleError = (state, action) => ({
    ...state,
    loading: false,
    error: action.error,
});

const handleFormFieldChange = (fieldName: string) => (state, action) => ({
    ...state,
    validationError: false,
    form: {
        ...state.form,
        [fieldName]: action.value,
    },
});

const handleSetValidationError = (state, action) => ({
    ...state,
    validationError: action.payload,
});

const handlers = {
    [types.MAIN.SUBMIT]: handleSubmit,
    [types.MAIN.SUCCESS]: handleSuccess,
    [types.MAIN.FAILURE]: handleError,
    [types.MAIN.FORM_FOOD_NAME_CHANGE]: handleFormFieldChange('foodName'),
    [types.MAIN.FORM_FOOD_WEIGHT_CHANGE]: handleFormFieldChange('foodWeight'),
    [types.MAIN.FORM_EAT_TIME_CHANGE]: handleFormFieldChange('eatTime'),
    [types.MAIN.SET_VALIDATION_ERROR]: handleSetValidationError,
};

export default function (state = initialState, action) {
    return handlers[action.type] ? handlers[action.type](state, action) : state;
}
