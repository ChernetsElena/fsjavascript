import * as types from '../../action-types';

type RecoveryCodeForm = {
    code: string;
};

export type RecoveryCodeState = {
    loading: boolean;
    data: { token: string };
    error: boolean | string;
    validationError: boolean | string;
    form: RecoveryCodeForm;
};

const initialState = {
    loading: false,
    data: null,
    error: false,
    validationError: false,
    form: {
        code: '',
    },
};

const handleSubmit = (state, action) => ({
    ...state,
    loading: true,
    error: false,
    form: { ...state.form },
});

const handleSuccess = (state, action) => ({
    ...initialState,
    form: { ...initialState.form },
    data: action.data,
});

const handleError = (state, action) => ({
    ...state,
    loading: false,
    error: action.error,
});

const handleCodeChange = (state, action) => ({
    ...state,
    validationError: false,
    form: {
        ...state.form,
        code: action.value,
    },
});

const handleSetValidationError = (state, action) => ({
    ...state,
    validationError: action.payload,
});

const handlers = {
    [types.RECOVERY_INPUT_CODE.SUBMIT]: handleSubmit,
    [types.RECOVERY_INPUT_CODE.SUCCESS]: handleSuccess,
    [types.RECOVERY_INPUT_CODE.FAILURE]: handleError,
    [types.RECOVERY_INPUT_CODE.FORM_CODE_CHANGE]: handleCodeChange,
    [types.RECOVERY_INPUT_CODE.SET_VALIDATION_ERROR]: handleSetValidationError,
};

export default function (state = initialState, action) {
    return handlers[action.type] ? handlers[action.type](state, action) : state;
}
