import * as types from '../../action-types';

type RecoveryPasswordForm = {
    password: string;
    passwordRepeate: string;
};

export type RecoveryPasswordState = {
    loading: boolean;
    data: { token: string };
    error: boolean | string;
    validationError: boolean | string;
    form: RecoveryPasswordForm;
};

const initialState = {
    loading: false,
    data: null,
    error: false,
    validationError: false,
    form: {
        password: '',
        passwordRepeat: '',
    },
};

const handleSubmit = (state, action) => ({
    ...state,
    loading: true,
    error: false,
    form: { ...state.form },
});

const handleSuccess = (state, action) => ({
    ...initialState,
    form: { ...initialState.form },
    data: action.data,
});

const handleError = (state, action) => ({
    ...state,
    loading: false,
    error: action.error,
});

const handleFormFieldChange = (fieldName: string) => (state, action) => ({
    ...state,
    validationError: false,
    form: {
        ...state.form,
        [fieldName]: action.value,
    },
});

const handleSetValidationError = (state, action) => ({
    ...state,
    validationError: action.payload,
});

const handlers = {
    [types.RECOVERY_INPUT_PASSWORD.SUBMIT]: handleSubmit,
    [types.RECOVERY_INPUT_PASSWORD.SUCCESS]: handleSuccess,
    [types.RECOVERY_INPUT_PASSWORD.FAILURE]: handleError,
    [types.RECOVERY_INPUT_PASSWORD.FORM_PASSWORD_CHANGE]: handleFormFieldChange(
        'password'
    ),
    [types.RECOVERY_INPUT_PASSWORD
        .FORM_PASSWORD_REPEAT_CHANGE]: handleFormFieldChange('passwordRepeat'),
    [types.RECOVERY_INPUT_PASSWORD
        .SET_VALIDATION_ERROR]: handleSetValidationError,
};

export default function (state = initialState, action) {
    return handlers[action.type] ? handlers[action.type](state, action) : state;
}
