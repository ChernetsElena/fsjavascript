import * as types from '../../action-types';

type RecoveryEmailForm = {
    email: string;
};

export type RecoveryEmailState = {
    loading: boolean;
    data: { token: string };
    error: boolean | string;
    validationError: boolean | string;
    form: RecoveryEmailForm;
};

const initialState = {
    loading: false,
    data: null,
    error: false,
    validationError: false,
    form: {
        email: '',
    },
};

const handleSubmit = (state, action) => ({
    ...state,
    loading: true,
    error: false,
    form: { ...state.form },
});

const handleSuccess = (state, action) => ({
    ...initialState,
    form: { ...initialState.form },
    data: action.data,
});

const handleError = (state, action) => ({
    ...state,
    loading: false,
    error: action.error,
});

const handleEmailChange = (state, action) => ({
    ...state,
    validationError: false,
    form: {
        ...state.form,
        email: action.value,
    },
});

const handleSetValidationError = (state, action) => ({
    ...state,
    validationError: action.payload,
});

const handlers = {
    [types.RECOVERY_INPUT_EMAIL.SUBMIT]: handleSubmit,
    [types.RECOVERY_INPUT_EMAIL.SUCCESS]: handleSuccess,
    [types.RECOVERY_INPUT_EMAIL.FAILURE]: handleError,
    [types.RECOVERY_INPUT_EMAIL.FORM_EMAIL_CHANGE]: handleEmailChange,
    [types.RECOVERY_INPUT_EMAIL.SET_VALIDATION_ERROR]: handleSetValidationError,
};

export default function (state = initialState, action) {
    return handlers[action.type] ? handlers[action.type](state, action) : state;
}
