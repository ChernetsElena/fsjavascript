import * as types from '../../action-types';

type RegisterDataForm = {
    userName: string;
    password: string;
    passwordRepeat;
    login: string;
};

export type RegisterDataState = {
    loading: boolean;
    data: { token: string };
    error: boolean | string;
    validationError: boolean | string;
    form: RegisterDataForm;
};

const initialState = {
    loading: false,
    data: null,
    error: false,
    validationError: false,
    form: {
        email: '',
        password: '',
        passwordRepeat: '',
        userName: '',
    },
};

const handleSubmit = (state, action) => ({
    ...state,
    loading: true,
    error: false,
    form: { ...state.form },
});

const handleSuccess = (state, action) => ({
    ...initialState,
    form: { ...initialState.form },
    data: action.data,
});

const handleError = (state, action) => ({
    ...state,
    loading: false,
    error: action.error,
});

const handleFormFieldChange = (fieldName: string) => (state, action) => ({
    ...state,
    validationError: false,
    form: {
        ...state.form,
        [fieldName]: action.value,
    },
});

const handleSetValidationError = (state, action) => ({
    ...state,
    validationError: action.payload,
});

const handlers = {
    [types.REGISTER_INPUT_DATA.SUBMIT]: handleSubmit,
    [types.REGISTER_INPUT_DATA.SUCCESS]: handleSuccess,
    [types.REGISTER_INPUT_DATA.FAILURE]: handleError,
    [types.REGISTER_INPUT_DATA.FORM_USERNAME_CHANGE]: handleFormFieldChange(
        'userName'
    ),
    [types.REGISTER_INPUT_DATA.FORM_EMAIL_CHANGE]: handleFormFieldChange(
        'email'
    ),
    [types.REGISTER_INPUT_DATA.FORM_PASSWORD_CHANGE]: handleFormFieldChange(
        'password'
    ),
    [types.REGISTER_INPUT_DATA
        .FORM_PASSWORD_REPEAT_CHANGE]: handleFormFieldChange('passwordRepeat'),
    [types.REGISTER_INPUT_DATA.SET_VALIDATION_ERROR]: handleSetValidationError,
};

export default function (state = initialState, action) {
    return handlers[action.type] ? handlers[action.type](state, action) : state;
}
