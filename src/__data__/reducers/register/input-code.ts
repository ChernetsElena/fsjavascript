import * as types from '../../action-types';

type RegisterCodeForm = {
    code: string;
};

export type RegisterCodeState = {
    loading: boolean;
    data: { token: string };
    error: boolean | string;
    validationError: boolean | string;
    form: RegisterCodeForm;
};

const initialState = {
    loading: false,
    data: null,
    error: false,
    validationError: false,
    form: {
        code: '',
    },
};

const handleSubmit = (state, action) => ({
    ...state,
    loading: true,
    error: false,
    form: { ...state.form },
});

const handleSuccess = (state, action) => ({
    ...initialState,
    form: { ...initialState.form },
    data: action.data,
});

const handleError = (state, action) => ({
    ...state,
    loading: false,
    error: action.error,
});

const handleCodeChange = (state, action) => ({
    ...state,
    validationError: false,
    form: {
        ...state.form,
        code: action.value,
    },
});

const handleSetValidationError = (state, action) => ({
    ...state,
    validationError: action.payload,
});

const handlers = {
    [types.REGISTER_INPUT_CODE.SUBMIT]: handleSubmit,
    [types.REGISTER_INPUT_CODE.SUCCESS]: handleSuccess,
    [types.REGISTER_INPUT_CODE.FAILURE]: handleError,
    [types.REGISTER_INPUT_CODE.FORM_CODE_CHANGE]: handleCodeChange,
    [types.REGISTER_INPUT_CODE.SET_VALIDATION_ERROR]: handleSetValidationError,
};

export default function (state = initialState, action) {
    return handlers[action.type] ? handlers[action.type](state, action) : state;
}
