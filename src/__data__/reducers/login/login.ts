import * as types from '../../action-types';

type LoginForm = {
    userName: string;
    password: string;
};

export type LoginState = {
    loading: boolean;
    data: string;
    error: boolean | string;
    validationError: boolean | string;
    form: LoginForm;
};

const initialState = {
    loading: false,
    data: '',
    error: false,
    validationError: false,
    form: {
        userName: '',
        password: '',
    },
};

const handleSubmit = (state, action) => ({
    ...state,
    loading: true,
    error: false,
    form: { ...state.form },
});

const handleSuccess = (state, action) => ({
    ...initialState,
    form: { ...initialState.form },
    data: action.data,
});

const handleError = (state, action) => ({
    ...state,
    loading: false,
    error: action.error,
});

const handleFormFieldChange = (fieldName: string) => (state, action) => ({
    ...state,
    validationError: false,
    form: {
        ...state.form,
        [fieldName]: action.value,
    },
});

const handleSetValidationError = (state, action) => ({
    ...state,
    validationError: action.payload,
});

const handlers = {
    [types.LOGIN.SUBMIT]: handleSubmit,
    [types.LOGIN.SUCCESS]: handleSuccess,
    [types.LOGIN.FAILURE]: handleError,
    [types.LOGIN.FORM_USERNAME_CHANGE]: handleFormFieldChange('userName'),
    [types.LOGIN.FORM_PASSWORD_CHANGE]: handleFormFieldChange('password'),
    [types.LOGIN.SET_VALIDATION_ERROR]: handleSetValidationError,
};

export default function (state = initialState, action) {
    return handlers[action.type] ? handlers[action.type](state, action) : state;
}
