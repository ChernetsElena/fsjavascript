import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';

import reducer from '../reducers';

const composeEnhancers = composeWithDevTools({});

export const store = createStore(
    reducer,
    composeEnhancers(applyMiddleware(thunk))
);
