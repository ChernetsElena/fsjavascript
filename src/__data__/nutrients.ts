export const nutrientsLeft = [
    { text: 'Na', name: 'Na', percent: 0 },
    { text: 'K', name: 'K', percent: 0 },
    { text: 'Ca', name: 'Ca', percent: 0 },
    { text: 'Mg', name: 'Mg', percent: 0 },
    { text: 'C', name: 'C', percent: 0 },
];

export const nutrientsRight = [
    { text: 'P', name: 'P', percent: 0 },
    { text: 'Fe', name: 'Fe', percent: 0 },
    { text: 'A', name: 'A', percent: 0 },
    { text: 'b-car', name: 'bcar', percent: 0 },
    { text: 'I', name: 'I', percent: 0 },
];

export const macronutrients = [
    { text: 'Калории', name: 'calories', percent: 0 },
    { text: 'Белки', name: 'proteins', percent: 0 },
    { text: 'Жиры', name: 'fats', percent: 0 },
    { text: 'Углеводы', name: 'carbohydrates', percent: 0 },
    { text: 'Вода', name: 'water', percent: 0 },
    { text: 'Клетчатка', name: 'cellulose', percent: 0 },
];
