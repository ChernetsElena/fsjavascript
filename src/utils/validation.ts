﻿import { useMemo } from 'react';
import _ from 'lodash';
import i18next from 'i18next';

type ValidatorObject<T> = {
    check(value: T): boolean;
    getMessage(value?: T): string;
};

export const twoSameValue = (
    itemName
): ValidatorObject<[unknown, unknown]> => ({
    check([value1, value2]) {
        return value1 === value2;
    },
    getMessage() {
        return i18next.t('microeat.validation.passwords', {
            itemName,
        });
    },
});

export const requireSpesialSymbols = (): ValidatorObject<string> => ({
    check(value) {
        return /[!?\-&_]/.test(value);
    },
    getMessage() {
        return i18next.t('microeat.validation.spesialsymbols');
    },
});

export const minMaxValidator = (
    itemName: string,
    min: number,
    max = Infinity
): ValidatorObject<string> => ({
    check(value) {
        return value.length >= min && value.length < max;
    },
    getMessage(value) {
        if (max === Infinity) {
            return i18next.t('microeat.validation.minmaxvalidator', {
                itemName,
                min,
            });
        }
        return i18next.t('microeat.validation.minmaxvalidator.withmax', {
            itemName,
            min,
            max,
        });
    },
});

export const emailValidator = (): ValidatorObject<string> => ({
    check(value): boolean {
        return /^[\w.-]{2,64}@[a-z]{2,16}\.[a-z]{2,3}$/i.test(value);
    },
    getMessage(): string {
        return i18next.t('microeat.validation.emailvalidator');
    },
});

type ValidationResult = {
    isValid: boolean;
    message: string;
};

const validate = <T>(
    value: T,
    validatorsArray: Array<ValidatorObject<T>>
): ValidationResult[] =>
    _.map(validatorsArray, (v) => ({
        isValid: v.check(value),
        message: v.getMessage(value),
    }));

export const useValidation = <T>(
    value: T,
    validatorsArray: Array<ValidatorObject<T>>
) => {
    return useMemo(() => {
        return validate(value, validatorsArray);
    }, [value]);
};

export const checkIsValid = (validatorsResults: ValidationResult[]) =>
    validatorsResults.findIndex((v) => !v.isValid) === -1;
export const getFirstError = (validatorsResults: ValidationResult[]) =>
    validatorsResults.find((v) => !v.isValid)?.message;

type useFormValidation = <T extends { [key: string]: Array<ValidationResult> }>(
    obj: T
) => {
    isFormValid: boolean;
    errors: { [key: string]: string };
};

export const useFormValidation: useFormValidation = (obj) => ({
    isFormValid:
        _.flatten(Object.values(obj)).findIndex((v) => !v.isValid) === -1,
    errors: Object.entries(obj)
        .map(([fieldName, validators]) => [
            fieldName,
            getFirstError(validators),
        ])
        .reduce(
            (acc, [fieldName, error]) => ({
                ...acc,
                [fieldName]: error,
            }),
            {}
        ),
});
