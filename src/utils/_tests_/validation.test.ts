﻿import i18next from 'i18next';
import {
    twoSameValue,
    requireSpesialSymbols,
    minMaxValidator,
    emailValidator,
} from '../validation';

import { describe, it, expect } from '@jest/globals';

describe('Проверка валидатора twoSameValue', () => {
    it('Строгое сравнение', () => {
        const validator = twoSameValue('test');
        expect(validator.check([1, '1'])).toBe(false);
        expect(validator.check([null, 0])).toBe(false);
        expect(validator.check([2, 2])).toBe(true);
    });

    it('Выводит ошибку с прокинутым значением', () => {
        const validator = twoSameValue('test');
        expect(validator.getMessage(['1', '2'])).toBe(
            i18next.t('microeat.validation.passwords', {
                itemName: 'test',
            })
        );
    });
});

describe('Проверка валидатора requireSpesialSymbols', () => {
    it('Проверяет наличие символа', () => {
        const validator = requireSpesialSymbols();
        expect(validator.check('12rt!')).toBe(true);
        expect(validator.check('12_rt')).toBe(true);
        expect(validator.check('LLrt&')).toBe(true);
        expect(validator.check('12Tt')).toBe(false);
    });

    it('Выводит ошибку', () => {
        const validator = requireSpesialSymbols();
        expect(validator.getMessage('12rt')).toBe(
            i18next.t('microeat.validation.spesialsymbols')
        );
    });
});

describe('Проверка валидатора minMaxValidator', () => {
    it('Проверяет, подходит ли длина строки ', () => {
        const validator = minMaxValidator('test', 6, 10);
        expect(validator.check('qwert')).toBe(false);
        expect(validator.check('qwerty')).toBe(true);
        expect(validator.check('qwerty12345&')).toBe(false);
    });

    it('Выводит ошибку когда не указан максимум', () => {
        const validator = minMaxValidator('test', 6);
        expect(validator.getMessage('12rt')).toBe(
            i18next.t('microeat.validation.minmaxvalidator', {
                itemName: 'test',
                min: 6,
            })
        );
    });

    it('Выводит ошибку когда указан максимум', () => {
        const validator = minMaxValidator('test', 6, 10);
        expect(validator.getMessage('12rt')).toBe(
            i18next.t('microeat.validation.minmaxvalidator.withmax', {
                itemName: 'test',
                min: 6,
                max: 10,
            })
        );
    });
});

describe('Проверка валидатора emailValidator', () => {
    it('Проверяет валидна ли почта', () => {
        const validator = emailValidator();
        expect(validator.check('12rt!')).toBe(false);
        expect(validator.check('weff-eeee@mail.ru')).toBe(true);
        expect(validator.check('weff.eee@3ewt.ru')).toBe(false);
        expect(validator.check('12233@mail.ru')).toBe(true);
    });

    it('Выводит ошибку', () => {
        const validator = emailValidator();
        expect(validator.getMessage('12rt')).toBe(
            i18next.t('microeat.validation.emailvalidator')
        );
    });
});
