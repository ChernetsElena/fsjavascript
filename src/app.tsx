﻿import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Dashboard from './containers/dashboard';
import './app.css';
import { Provider } from 'react-redux';
import { store } from './__data__/store';

const App = () => (
    <Provider store={store}>
        <BrowserRouter basename={'/fsjavascript'}>
            <Dashboard />
        </BrowserRouter>
    </Provider>
);

export default App;
