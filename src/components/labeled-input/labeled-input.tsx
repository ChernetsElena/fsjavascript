﻿import React, { HTMLAttributes } from 'react';
import { RefactorActionInfo } from 'typescript';
import PropTypes from 'prop-types';
import style from './style.css';
import cn from 'classnames';

interface LabeledInputProps
    extends Omit<
        React.DetailedHTMLProps<
            React.InputHTMLAttributes<HTMLInputElement>,
            HTMLInputElement
        >,
        'id'
    > {
    id: string | number;
    name: string;
    text?: string;
    type?: string;
    inputRef?: React.RefObject<HTMLInputElement>;
    error?: string | boolean;
    className?: string;
}

export const LabeledInput: React.FC<LabeledInputProps> = ({
    inputRef,
    text,
    id,
    name,
    type,
    error,
    className,
    ...rest
}) => (
    <div className={cn(style.main, className)}>
        {text && (
            <label className={cn(style.label, className)} htmlFor={String(id)}>
                {text}
            </label>
        )}
        <input
            className={style.input}
            ref={inputRef}
            type={type}
            name={name}
            id={String(id)}
            {...rest}
        />
        {error && typeof error != 'boolean' && (
            <div className={style.error}>
                <span>{error}</span>
            </div>
        )}
    </div>
);

LabeledInput.propTypes = {
    text: PropTypes.string.isRequired,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    name: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['text', 'password']),
    error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
};

LabeledInput.defaultProps = {
    type: 'text',
    error: void 0,
};
