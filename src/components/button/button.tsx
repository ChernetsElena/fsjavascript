﻿import React from 'react';
import cn from 'classnames';
import style from './style.css';
import { ButtonColors } from './model';

interface ButtonProps
    extends React.DetailedHTMLProps<
        React.ButtonHTMLAttributes<HTMLButtonElement>,
        HTMLButtonElement
    > {
    colorScheme: ButtonColors;
    onClick?: any;
    className?: string;
}

const Button: React.FC<ButtonProps> = ({
    colorScheme,
    children,
    className,
    ...rest
}) => (
    <button className={cn(style.main, style[colorScheme], className)} {...rest}>
        {children}
    </button>
);

export default Button;
