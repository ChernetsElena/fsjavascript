import React from 'react';
import PropTypes from 'prop-types';
import style from './style.css';

interface Nutrient {
    text?: string;
    name?: string;
    percent?: number;
}

export const Nutrient = ({ text, percent }) => (
    <div className={style.nutrient}>
        {text && <div className={style.nameNutrient}>{text}</div>}
        <svg className={style.svg}>
            <rect className={style.baseRect} />
            <rect width={percent} className={style.percentRect} />
        </svg>
        {percent + 1 && <div className={style.percentNutrient}>{percent}%</div>}
    </div>
);

Nutrient.propTypes = {
    text: PropTypes.string.isRequired,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    name: PropTypes.string.isRequired,
    percent: PropTypes.number.isRequired,
};

Nutrient.defaultProps = {
    percent: 0,
    text: 'Mg',
};
