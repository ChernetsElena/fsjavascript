﻿import React from 'react';
import { mount } from 'enzyme';

import Button, { ButtonColors } from '../button';
import { describe, it, expect } from '@jest/globals';

describe('<Button />', () => {
    it('Монтируется и отрисовывается', () => {
        const wrapper = mount(<Button colorScheme={ButtonColors.blue} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('Пробрасывает className', () => {
        const wrapper = mount(
            <Button colorScheme={ButtonColors.blue} className="Доп. стиль" />
        );

        expect(wrapper).toMatchSnapshot();
    });
});
