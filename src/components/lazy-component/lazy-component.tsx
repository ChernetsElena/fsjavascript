import React, { Suspense } from 'react';
import ErrorBoundary from '../error-boundary';

const LazyComponent = ({ children, fallback }) => {
    return (
        <ErrorBoundary>
            <Suspense fallback={fallback}>{children}</Suspense>
        </ErrorBoundary>
    );
};

LazyComponent.defaultProps = {
    fallback: 'Пожалуйста, подождите',
};

export default LazyComponent;
