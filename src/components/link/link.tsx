﻿import React from 'react';
import cn from 'classnames';
import style from './style.css';
import { reduceEachLeadingCommentRange } from 'typescript';

enum LinkColors {
    red = 'red',
}

type LinkProps = {
    colorScheme?: LinkColors;
    href: string;
    className?: string;
    as?: any;
    type?: 'link' | 'button';
};

export type LinkType = React.FC<LinkProps>;

export const Link: LinkType = ({
    href,
    colorScheme,
    children,
    className,
    as: LinkComponent,
}) => {
    return (
        <LinkComponent
            className={cn(style.main, style[colorScheme], className)}
            href={href}
        >
            {children}
        </LinkComponent>
    );
};

Link.defaultProps = {
    colorScheme: LinkColors.red,
    type: 'link',
    as: 'a',
};
