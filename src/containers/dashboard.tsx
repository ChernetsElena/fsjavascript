﻿import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import LazyComponent from '../components/lazy-component';
import { URLs } from '../__data__/urls';

import Login from './login';
import Register from './register';
import Recovery from './recovery';
import Main from './main';

const Loader = () => <div>Пожалуйста, подождите..</div>;

const Dashboard = () => (
    <Switch>
        <Route exact path={URLs.root.url}>
            <Redirect to={URLs.login.url} />
        </Route>
        <Route path={URLs.login.url}>
            <LazyComponent>
                <Login />
            </LazyComponent>
        </Route>
        <Route path={URLs.register.url}>
            <LazyComponent>
                <Register />
            </LazyComponent>
        </Route>
        <Route path={URLs.recovery.url}>
            <LazyComponent>
                <Recovery />
            </LazyComponent>
        </Route>
        <Route path={URLs.main.url}>
            <LazyComponent>
                <Main />
            </LazyComponent>
        </Route>
        <Route path="*">
            <Redirect to={URLs.login.url} />
        </Route>
    </Switch>
);

export default Dashboard;
