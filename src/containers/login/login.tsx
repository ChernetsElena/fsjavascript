import React, { useEffect, useRef, useState } from 'react';
import style from './style.css';
import cn from 'classnames';
import {
    LabeledInput,
    Button,
    ButtonColors,
    ErrorBoundary,
    Link,
} from '../../components';
import { URLs, baseUrl } from '../../__data__/urls';
import { Redirect } from 'react-router-dom';
import {
    submitLoginForm,
    formUserNameChange as formUserNameChangeAction,
    formPasswordChange as formPasswordChangeAction,
    setValidationError as setValidationErrorAction,
} from '../../__data__/actions/login';
import { connect } from 'react-redux';
import i18next from 'i18next';

type MapStateToProps = {
    loading: boolean;
    data: any;
    asyncError: boolean | string;
    validationError: boolean | string;
    userName: string;
    password: string;
};

type MapDispatchToProps = {
    submitLogin: (userName: string, password: string) => void;
    setUserName: (value: string) => void;
    setPassword: (value: string) => void;
    setValidationError: (error: string) => void;
};

type LoginProps = MapStateToProps & MapDispatchToProps;

function Login({
    data,
    userName,
    password,
    submitLogin,
    setUserName,
    setPassword,
    validationError,
    setValidationError,
    asyncError,
}: React.PropsWithChildren<LoginProps>) {
    const loginRef = useRef(null);
    const [needRedirect, setNeedRedirect] = useState(false);

    useEffect(() => {
        loginRef.current.focus();
    }, []);

    useEffect(() => {
        if (data) {
            setNeedRedirect(true);
        }
    }, [data]);

    function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        if (!userName || !password) {
            setValidationError(i18next.t('microeat.login.validation.error'));
            loginRef.current.focus();
            return;
        }
        submitLogin(userName, password);
    }

    if (needRedirect) {
        return <Redirect to={URLs.main.url} />;
    }

    function handleUserNameChange(event) {
        setUserName(event.target.value);
    }

    function handlePasswordChange(event) {
        setPassword(event.target.value);
    }

    return (
        <ErrorBoundary>
            <div className={style.wrapper}>
                <form className={style.loginForm} onSubmit={handleSubmit}>
                    <h2 className={style.loginTitleSpacing}>
                        {i18next.t('microeat.form.microeat')}
                    </h2>
                    <div className={style.loginInner}>
                        <h3 className={style.loginTitle}>
                            {i18next.t('microeat.login.header')}
                        </h3>
                        <LabeledInput
                            inputRef={loginRef}
                            text={i18next.t(
                                'microeat.login.username.input.text'
                            )}
                            id="login-input"
                            name="login"
                            value={userName}
                            onChange={handleUserNameChange}
                            error={!!asyncError || !!validationError}
                        />
                        <LabeledInput
                            text={i18next.t(
                                'microeat.login.password.input.text'
                            )}
                            id="password-input"
                            name="password"
                            type="password"
                            value={password}
                            onChange={handlePasswordChange}
                            error={asyncError || validationError}
                        />
                        <div className={style.loginInnerLine}>
                            <Button
                                id="loginSubmitButton"
                                type="submit"
                                colorScheme={ButtonColors.green}
                                className={style.bigButton}
                            >
                                {i18next.t('microeat.login.button.submit')}
                            </Button>
                            <div className={style.cancel}>
                                <Link
                                    href={baseUrl + URLs.recovery.url}
                                    className={cn(
                                        style.loginLink,
                                        style.loginForgot
                                    )}
                                >
                                    {i18next.t('microeat.login.link.recovery')}
                                </Link>
                            </div>
                        </div>

                        <div className={style.loginRegister}>
                            <Link
                                href={baseUrl + URLs.register.url}
                                className={style.loginLink}
                            >
                                {i18next.t('microeat.login.link.register')}
                            </Link>
                        </div>
                    </div>
                </form>
            </div>
        </ErrorBoundary>
    );
}

const mapStateToProps = (state): MapStateToProps => ({
    loading: state.login.loading,
    data: state.login.data,
    asyncError: state.login.error,
    validationError: state.login.validationError,
    ...state.login.form,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
    submitLogin: (userName, password) =>
        dispatch(submitLoginForm({ userName, password })),
    setValidationError: (error) => dispatch(setValidationErrorAction(error)),
    setUserName: (value) => dispatch(formUserNameChangeAction(value)),
    setPassword: (value) => dispatch(formPasswordChangeAction(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
