﻿import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';

import Recovery from '../recovery/recovery';

import { describe, it, expect } from '@jest/globals';

describe('Тестирование контейнера Recovery', () => {
    it('Тест рендер Recovery', () => {
        expect.assertions(1);
        const component = mount(
            <Provider store={store}>
                <Recovery />)
            </Provider>
        );
        expect(component).toMatchSnapshot();
    });
});
