﻿import React from 'react';
import { mount } from 'enzyme';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';
import mockAdapter from 'axios-mock-adapter';

import axios from 'axios';
import Login from '../login/login';
import { describe, it, expect, beforeEach } from '@jest/globals';

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift();

            if (config.url.includes(url)) {
                return response;
            }
        });
    });
};

describe('Тестирование контейнера Login', () => {
    let mock;
    beforeEach(() => {
        mock = new mockAdapter(axios);
    });
    it('Тест рендер Login', async () => {
        expect.assertions(5);
        //первый рендер
        const component = mount(
            <Provider store={store}>
                <Login />)
            </Provider>
        );
        expect(component).toMatchSnapshot();

        //проверка валидации на пустые поля

        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //пользователь вводит логин и пароль
        component.find('input#login-input').simulate('change', {
            target: {
                value: 'test login',
            },
        });
        component.find('input#password-input').simulate('change', {
            target: {
                value: 'test password',
            },
        });
        component.update();
        expect(component).toMatchSnapshot();

        //логин с заполненными полями
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        const response = [
            [
                'POST',
                '/login',
                {},
                200,
                {
                    token: 'test token',
                },
            ],
        ];

        //перехват запроса
        await multipleRequest(mock, response);
        component.update();
        expect(component).toMatchSnapshot();
    });
});
