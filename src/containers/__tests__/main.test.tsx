﻿import React from 'react';
import { mount } from 'enzyme';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';
import mockAdapter from 'axios-mock-adapter';
import axios from 'axios';
import Main from '../main/main';

import { describe, it, expect, beforeEach } from '@jest/globals';

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift();

            if (config.url.includes(url)) {
                return response;
            }
        });
    });
};

describe('Тестирование контейнера Main', () => {
    let mock;
    beforeEach(() => {
        mock = new mockAdapter(axios);
    });
    it('Тест рендер Main', async () => {
        expect.assertions(5);
        //первый рендер
        const component = mount(
            <Provider store={store}>
                <Main />)
            </Provider>
        );
        expect(component).toMatchSnapshot();

        //проверка валидации на пустые поля
        component.find('button#add-eat').simulate('click');
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //проверка валидации на пустые поля для show-nutrients
        component.find('button#show-nutrients').simulate('click');
        component.update();
        expect(component).toMatchSnapshot();

        //проверка валидации названия продукта и перехват запроса с ошибкой 3
        component.find('input#food-name-input').simulate('change', {
            target: {
                value: 'test foodName',
            },
        });
        component.find('input#food-weight-input').simulate('change', {
            target: {
                value: '100',
            },
        });
        component.find('input#eat-time-input').simulate('change', {
            target: {
                value: 'Завтрак',
            },
        });

        component.find('form').simulate('submit');

        const response = [
            [
                'POST',
                '/main/addfood',
                {},
                2,
                {
                    error: 'Продукт не найден',
                },
            ],
        ];

        await multipleRequest(mock, response);
        component.update();
        expect(component).toMatchSnapshot();

        ////проверка валидации названия продукта и перехват запроса с ошибкой 4
        component.find('input#food-name-input').simulate('change', {
            target: {
                value: 'test',
            },
        });

        component.find('button#show-nutrients').simulate('click');

        const responseData = [
            [
                'POST',
                '/main/showfoodnutrients',
                {},
                200,
                {
                    error: 'Продукт не найден',
                },
            ],
        ];

        await multipleRequest(mock, responseData);
        component.update();
        expect(component).toMatchSnapshot();
    });
});
