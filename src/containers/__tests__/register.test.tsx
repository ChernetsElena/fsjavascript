﻿import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { store } from '../../__data__/store';

import Register from '../register/register';

import { describe, it, expect } from '@jest/globals';

describe('Тестирование контейнера Register', () => {
    it('Тест рендер Register', () => {
        expect.assertions(1);
        const component = mount(
            <Provider store={store}>
                <Register />)
            </Provider>
        );
        expect(component).toMatchSnapshot();
    });
});
