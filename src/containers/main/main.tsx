import {
    Button,
    ButtonColors,
    ErrorBoundary,
    LabeledInput,
    Nutrient,
} from '../../components';
import React, { useEffect, useState } from 'react';
import style from './style.css';
import cn from 'classnames';
import {
    nutrientsLeft,
    nutrientsRight,
    macronutrients,
} from '../../__data__/nutrients';
import { foodList } from '../../__data__/food-list';
import i18next from 'i18next';
import { connect } from 'react-redux';
import {
    submitMainFoodForm,
    formFoodWeightChange as formFoodWeightChangeAction,
    formFoodNameChange as formFoodNameChangeAction,
    formEatTimeChange as formEatTimeChangeAction,
    setValidationError as setValidationErrorAction,
} from '../../__data__/actions/main-add-food';

import { submitFoodNutrients } from '../../__data__/actions/show-food-nutrients';

type MapStateToProps = {
    loading: boolean;
    data: any;
    dataShow: any;
    asyncError: boolean | string;
    asyncErrorShow: boolean | string;
    validationError: boolean | string;
    foodName: string;
    foodWeight: string;
    eatTime: string;
};

type MapDispatchToProps = {
    submitFoodForm: (
        foodName: string,
        foodWeight: string,
        eatTime: string,
        complete: boolean
    ) => void;
    setFoodName: (value: string) => void;
    setFoodWeight: (value: string) => void;
    setEatTime: (value: string) => void;
    setValidationError: (error: string) => void;
    submitFoodNutrients: (foodName: string) => void;
};

type MainProps = MapStateToProps & MapDispatchToProps;

function Main({
    data,
    dataShow,
    foodName,
    foodWeight,
    eatTime,
    submitFoodForm,
    submitFoodNutrients,
    setFoodName,
    setFoodWeight,
    setEatTime,
    validationError,
    setValidationError,
    asyncError,
    asyncErrorShow,
}: React.PropsWithChildren<MainProps>) {
    const [showError, setShowError] = useState(false);
    const [inputAddEat, setInputAddEat] = useState(false);
    const [showEatNutrients, setShowEatNutrients] = useState(false);
    const [showPercent, setShowPercent] = useState(false);
    const [showDay, setShowDay] = useState(true);
    const [showBreakfast, setShowBreakfast] = useState(false);
    const [showDinner, setShowDinner] = useState(false);
    const [showLunch, setShowLunch] = useState(false);

    useEffect(() => {
        if (dataShow) {
            setShowEatNutrients(true);
        }
    }, [dataShow]);

    useEffect(() => {
        if (asyncError) {
            setShowError(true);
        }
    }, [asyncError]);

    useEffect(() => {
        if (asyncErrorShow) {
            setShowError(true);
        }
    }, [asyncErrorShow]);

    useEffect(() => {
        if (data) {
            setShowPercent(true);
        }
    }, [data]);

    function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        if (!foodName || !foodWeight || !eatTime) {
            setValidationError(i18next.t('microeat.main.validation.error'));
            setShowError(true);
            return;
        }
        submitFoodForm(foodName, foodWeight, eatTime, false);
    }

    function handleSubmitComplete() {
        submitFoodForm(foodName, foodWeight, eatTime, true);
    }

    function handleSubmitShowNutrients(
        event: React.FormEvent<HTMLFormElement>
    ) {
        event.preventDefault();
        if (!foodName) {
            setValidationError(
                i18next.t('microeat.main.show.nutrients.validation.error')
            );
            setShowError(true);
            return;
        }
        submitFoodNutrients(foodName);
    }

    function handleFoodNameChange(event) {
        setShowError(false);
        setFoodName(event.target.value);
    }

    function handleFoodWeightChange(event) {
        setShowError(false);
        setFoodWeight(event.target.value);
    }

    function handleEatTimeChange(event) {
        setShowError(false);
        setEatTime(event.target.value);
    }

    function handleInputAddEat() {
        setInputAddEat(true);
    }

    function handleCancelInputAddEat() {
        setInputAddEat(false);
    }

    function handleCancelShowEatNutrients() {
        setShowEatNutrients(false);
    }

    function handleShowDay() {
        setShowDay(true);
        setShowDinner(false);
        setShowLunch(false);
        setShowBreakfast(false);
    }

    function handleShowBreakfast() {
        setShowDay(false);
        setShowDinner(false);
        setShowLunch(false);
        setShowBreakfast(true);
    }

    function handleShowLunch() {
        setShowDay(false);
        setShowDinner(false);
        setShowLunch(true);
        setShowBreakfast(false);
    }

    function handleShowDinner() {
        setShowDay(false);
        setShowDinner(true);
        setShowLunch(false);
        setShowBreakfast(false);
    }

    return (
        <ErrorBoundary>
            <div className={style.wrapper}>
                <header className={style.header}>
                    <h1 className={style.headerTitleSpacing}>
                        {i18next.t('microeat.main.microeat')}
                    </h1>
                </header>
                <div className={cn(style.main)}>
                    <Button
                        className={style.bigButton}
                        onClick={handleInputAddEat}
                        colorScheme={ButtonColors.red}
                        id={'add-eat'}
                    >
                        {i18next.t('microeat.main.button.add.product')}
                    </Button>
                    {inputAddEat && (
                        <form
                            className={style.addEatWrapper}
                            onSubmit={handleSubmit}
                        >
                            <div className={style.addEat}>
                                <div className={style.addEatInput}>
                                    <LabeledInput
                                        className={style.addEatLabeledInput}
                                        text={i18next.t(
                                            'microeat.main.input.text.product'
                                        )}
                                        id="food-name-input"
                                        name="foodNameInput"
                                        placeholder={i18next.t(
                                            'microeat.main.input.text.product.placeholder'
                                        )}
                                        list="foodName"
                                        onChange={handleFoodNameChange}
                                        value={foodName}
                                        error={
                                            showError &&
                                            (!!validationError || !!asyncError)
                                        }
                                    ></LabeledInput>
                                    <datalist id="foodName">
                                        {foodList.map((item) => (
                                            <option
                                                key={item}
                                                value={item}
                                            ></option>
                                        ))}
                                    </datalist>
                                    <LabeledInput
                                        className={style.addEatLabeledInput}
                                        text={i18next.t(
                                            'microeat.main.input.text.weight'
                                        )}
                                        id="food-weight-input"
                                        name="foodWeightInput"
                                        placeholder="100"
                                        onChange={handleFoodWeightChange}
                                        value={foodWeight}
                                        error={
                                            showError &&
                                            (!!asyncError || !!validationError)
                                        }
                                    ></LabeledInput>
                                    <LabeledInput
                                        className={style.addEatLabeledInput}
                                        text={i18next.t(
                                            'microeat.main.input.text.eating'
                                        )}
                                        id="eat-time-input"
                                        name="eatTimeInput"
                                        placeholder={i18next.t(
                                            'microeat.main.input.text.eating.placeholder'
                                        )}
                                        list="eatTime"
                                        onChange={handleEatTimeChange}
                                        value={eatTime}
                                        error={
                                            showError &&
                                            (asyncError ||
                                                asyncErrorShow ||
                                                validationError)
                                        }
                                    ></LabeledInput>
                                    <datalist id="eatTime">
                                        <option value="Завтрак"></option>
                                        <option value="Обед"></option>
                                        <option value="Ужин"></option>
                                    </datalist>
                                </div>
                                <div className={style.addEatButtons}>
                                    <Button
                                        type="submit"
                                        className={style.bigButton}
                                        colorScheme={ButtonColors.red}
                                    >
                                        {i18next.t('microeat.main.button.add')}
                                    </Button>
                                    <Button
                                        type="button"
                                        className={style.bigButton}
                                        colorScheme={ButtonColors.green}
                                        onClick={handleSubmitShowNutrients}
                                        id={'show-nutrients'}
                                    >
                                        {i18next.t(
                                            'microeat.main.button.show.nutrients'
                                        )}
                                    </Button>
                                </div>
                                <Button
                                    className={style.middleButton}
                                    onClick={handleCancelInputAddEat}
                                    colorScheme={ButtonColors.blue}
                                >
                                    {i18next.t('microeat.main.button.close')}
                                </Button>
                            </div>
                        </form>
                    )}

                    {showEatNutrients && (
                        <div className={style.showEatNutrientsWrapper}>
                            <div className={style.showEatNutrients}>
                                <h2 className={style.showEatNutrientsTitle}>
                                    {i18next.t(
                                        'microeat.main.show.nutrients.nutrients'
                                    )}
                                </h2>
                                <div className={style.vitamins}>
                                    <h3 className={style.showEatNutrientsH3}>
                                        {i18next.t(
                                            'microeat.main.show.nutrients.vitamins.and.minerals'
                                        )}
                                    </h3>
                                    <div className={style.vitaminWrapper}>
                                        <div className={style.nutrientWrapper}>
                                            {nutrientsLeft.map((item) => (
                                                <Nutrient
                                                    key={item.name}
                                                    text={item.text}
                                                    name={item.name}
                                                    percent={
                                                        dataShow[item.name]
                                                    }
                                                />
                                            ))}
                                        </div>
                                        <div className={style.nutrientWrapper}>
                                            {nutrientsRight.map((item) => (
                                                <Nutrient
                                                    key={item.name}
                                                    text={item.text}
                                                    name={item.name}
                                                    percent={
                                                        dataShow[item.name]
                                                    }
                                                />
                                            ))}
                                        </div>
                                    </div>
                                </div>
                                <div className={style.macronutrients}>
                                    <h3 className={style.showEatNutrientsH3}>
                                        {i18next.t(
                                            'microeat.main.show.nutrients.macronutrients'
                                        )}
                                    </h3>
                                    <div className={style.nutrientWrapper}>
                                        {macronutrients.map((item) => (
                                            <Nutrient
                                                key={item.name}
                                                text={item.text}
                                                name={item.name}
                                                percent={dataShow[item.name]}
                                            />
                                        ))}
                                    </div>
                                </div>
                                <Button
                                    className={style.middleButton}
                                    onClick={handleCancelShowEatNutrients}
                                    colorScheme={ButtonColors.blue}
                                >
                                    {i18next.t(
                                        'microeat.main.show.nutrients.button.close'
                                    )}
                                </Button>
                            </div>
                        </div>
                    )}

                    <div className={style.yourNutrients}>
                        <h2 className={style.showEatNutrientsTitle}>
                            {i18next.t('microeat.main.your.nutrients')}
                        </h2>
                        <div>
                            <Button
                                className={style.middleButton}
                                colorScheme={ButtonColors.green}
                                onClick={handleShowDay}
                            >
                                {i18next.t('microeat.main.button.day')}
                            </Button>
                            <Button
                                className={style.middleButton}
                                colorScheme={ButtonColors.green}
                                onClick={handleShowBreakfast}
                            >
                                {i18next.t('microeat.main.button.breakfast')}
                            </Button>
                            <Button
                                className={style.middleButton}
                                colorScheme={ButtonColors.green}
                                onClick={handleShowLunch}
                            >
                                {i18next.t('microeat.main.button.lunch')}
                            </Button>
                            <Button
                                className={style.middleButton}
                                colorScheme={ButtonColors.green}
                                onClick={handleShowDinner}
                            >
                                {i18next.t('microeat.main.button.dinner')}
                            </Button>
                        </div>
                        <div>
                            <div className={style.vitamins}>
                                <h3>
                                    {i18next.t(
                                        'microeat.main.vitamins.and.minerals'
                                    )}
                                </h3>
                                <div className={style.vitaminWrapper}>
                                    <div className={style.nutrientWrapper}>
                                        {nutrientsLeft.map((item) => (
                                            <Nutrient
                                                key={item.name}
                                                text={item.text}
                                                name={item.name}
                                                percent={
                                                    (showPercent &&
                                                        ((showDay &&
                                                            data.day[
                                                                item.name
                                                            ]) ||
                                                            (showBreakfast &&
                                                                data.breakfast[
                                                                    item.name
                                                                ]) ||
                                                            (showLunch &&
                                                                data.lunch[
                                                                    item.name
                                                                ]) ||
                                                            (showDinner &&
                                                                data.dinner[
                                                                    item.name
                                                                ]))) ||
                                                    0
                                                }
                                            />
                                        ))}
                                    </div>
                                    <div className={style.nutrientWrapper}>
                                        {nutrientsRight.map((item) => (
                                            <Nutrient
                                                key={item.name}
                                                text={item.text}
                                                name={item.name}
                                                percent={
                                                    (showPercent &&
                                                        ((showDay &&
                                                            data.day[
                                                                item.name
                                                            ]) ||
                                                            (showBreakfast &&
                                                                data.breakfast[
                                                                    item.name
                                                                ]) ||
                                                            (showLunch &&
                                                                data.lunch[
                                                                    item.name
                                                                ]) ||
                                                            (showDinner &&
                                                                data.dinner[
                                                                    item.name
                                                                ]))) ||
                                                    0
                                                }
                                            />
                                        ))}
                                    </div>
                                </div>
                            </div>
                            <div className={style.macronutrients}>
                                <h3>
                                    {i18next.t('microeat.main.macronutrients')}
                                </h3>
                                <div className={style.nutrientWrapper}>
                                    {macronutrients.map((item) => (
                                        <Nutrient
                                            key={item.name}
                                            text={item.text}
                                            name={item.name}
                                            percent={
                                                (showPercent &&
                                                    ((showDay &&
                                                        data.day[item.name]) ||
                                                        (showBreakfast &&
                                                            data.breakfast[
                                                                item.name
                                                            ]) ||
                                                        (showLunch &&
                                                            data.lunch[
                                                                item.name
                                                            ]) ||
                                                        (showDinner &&
                                                            data.dinner[
                                                                item.name
                                                            ]))) ||
                                                0
                                            }
                                        />
                                    ))}
                                </div>
                            </div>
                        </div>
                    </div>
                    <Button
                        className={style.bigButton}
                        onClick={handleSubmitComplete}
                        colorScheme={ButtonColors.green}
                    >
                        {i18next.t(
                            'microeat.main.show.nutrients.button.complete'
                        )}
                    </Button>
                </div>
            </div>
        </ErrorBoundary>
    );
}

const mapStateToProps = (state): MapStateToProps => ({
    loading: state.main.addEat.loading,
    data: state.main.addEat.data,
    dataShow: state.main.showNutrients.data,
    asyncError: state.main.addEat.error,
    asyncErrorShow: state.main.showNutrients.error,
    validationError: state.main.addEat.validationError,
    ...state.main.addEat.form,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
    submitFoodNutrients: (foodName) => dispatch(submitFoodNutrients(foodName)),
    submitFoodForm: (foodName, foodWeight, eatTime, complete) =>
        dispatch(
            submitMainFoodForm({ foodName, foodWeight, eatTime, complete })
        ),
    setValidationError: (error) => dispatch(setValidationErrorAction(error)),
    setFoodName: (value) => dispatch(formFoodNameChangeAction(value)),
    setFoodWeight: (value) => dispatch(formFoodWeightChangeAction(value)),
    setEatTime: (value) => dispatch(formEatTimeChangeAction(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
