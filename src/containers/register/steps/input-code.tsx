import React, { useEffect, useRef, useState } from 'react';
import { Redirect } from 'react-router-dom';
import style from './style.css';
import { LabeledInput, Button, ButtonColors, Link } from '../../../components';
import { StageProps } from './model';
import { URLs, baseUrl } from '../../../__data__/urls';
import {
    submitRegisterFormCode,
    formCodeChange as formCodeChangeAction,
    setValidationError as setValidationErrorAction,
} from '../../../__data__/actions/register-code';
import { connect } from 'react-redux';
import i18next from 'i18next';

type MapStateToProps = {
    loading: boolean;
    data: any;
    asyncError: boolean | string;
    validationError: boolean | string;
    code: string;
};

type MapDispatchToProps = {
    submitFormCode: (code: string) => void;
    setCode: (value: string) => void;
    setValidationError: (error: string) => void;
};

type InputCodeProps = StageProps & MapStateToProps & MapDispatchToProps;

function InputCode({
    data,
    code,
    submitFormCode,
    setCode,
    validationError,
    setValidationError,
    asyncError,
}: React.PropsWithChildren<InputCodeProps>) {
    const [needRedirect, setNeedRedirect] = useState(false);
    const [showError, setShowError] = useState(false);
    const firstInputRef = useRef(null);

    useEffect(function () {
        firstInputRef.current.focus();
    }, []);

    useEffect(() => {
        if (data) {
            setNeedRedirect(true);
        }
    }, [data]);

    useEffect(() => {
        if (asyncError) {
            setShowError(true);
        }
    }, [asyncError]);

    async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        if (!code) {
            setValidationError(
                i18next.t('microeat.register.code.validation.error')
            );
            setShowError(true);
            firstInputRef.current.focus();
            return;
        }
        submitFormCode(code);
    }

    function handleChangeCode(event) {
        setShowError(false);
        setCode(event.target.value);
    }

    if (needRedirect) {
        return <Redirect to={URLs.login.url} />;
    }

    return (
        <form className={style.loginForm} onSubmit={handleSubmit}>
            <h2 className={style.loginTitleSpacing}>
                {i18next.t('microeat.form.microeat')}
            </h2>
            <div className={style.loginInner}>
                <h3 className={style.loginTitle}>
                    {i18next.t('microeat.register.code.header')}
                </h3>
                <LabeledInput
                    inputRef={firstInputRef}
                    text={i18next.t('microeat.register.code.input.code.text')}
                    id="code-input"
                    name="code"
                    value={code}
                    onChange={handleChangeCode}
                    error={showError && (asyncError || validationError)}
                />
                <div className={style.button}>
                    <Button type="submit" colorScheme={ButtonColors.green}>
                        {i18next.t('microeat.register.code.button.submit')}
                    </Button>
                </div>
                <div className={style.cancel}>
                    <Link href={baseUrl + URLs.login.url}>
                        {i18next.t('microeat.register.code.button.cancel')}
                    </Link>
                </div>
            </div>
        </form>
    );
}

const mapStateToProps = (state): MapStateToProps => ({
    loading: state.register.inputCode.loading,
    data: state.register.inputCode.data,
    asyncError: state.register.inputCode.error,
    validationError: state.register.inputCode.validationError,
    ...state.register.inputCode.form,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
    submitFormCode: (code) => dispatch(submitRegisterFormCode({ code })),
    setValidationError: (error) => dispatch(setValidationErrorAction(error)),
    setCode: (value) => dispatch(formCodeChangeAction(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InputCode);
