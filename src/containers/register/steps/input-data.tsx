import React, { useEffect, useRef, useState } from 'react';
import { baseUrl, URLs } from '../../../__data__/urls';
import style from './style.css';
import { LabeledInput, Button, ButtonColors, Link } from '../../../components';
import { RegisterStep, StageProps } from './model';
import { connect } from 'react-redux';
import {
    minMaxValidator,
    useValidation,
    requireSpesialSymbols,
    twoSameValue,
    useFormValidation,
    emailValidator,
} from '../../../utils';
import {
    submitRegisterFormData,
    formPasswordChange as formPasswordChangeAction,
    formPasswordRepeatChange as formPasswordRepeatChangeAction,
    formEmailChange as formEmailChangeAction,
    formUserNameChange as formUserNameChangeAction,
    setValidationError as setValidationErrorAction,
} from '../../../__data__/actions/register-data';
import i18next from 'i18next';

type MapStateToProps = {
    loading: boolean;
    data: any;
    asyncError: boolean | string;
    validationError: boolean | string;
    email: string;
    userName: string;
    password: string;
    passwordRepeat: string;
};

type MapDispatchToProps = {
    submitFormData: (userName: string, password: string, email: string) => void;
    setUserName: (value: string) => void;
    setEmail: (value: string) => void;
    setPassword: (value: string) => void;
    setPasswordRepeat: (value: string) => void;
    setValidationError: (error: string) => void;
};

type InputDataProps = StageProps & MapStateToProps & MapDispatchToProps;

function InputData({
    setStep,
    data,
    submitFormData,
    email,
    password,
    passwordRepeat,
    userName,
    setEmail,
    setPassword,
    setPasswordRepeat,
    setUserName,
    validationError,
    setValidationError,
    asyncError,
}: React.PropsWithChildren<InputDataProps>) {
    const firstInputRef = useRef(null);
    const [showError, setShowError] = useState(false);

    useEffect(() => {
        firstInputRef.current.focus();
    }, []);

    useEffect(() => {
        if (data) {
            setStep(RegisterStep.CODE_STEP);
        }
    }, [data]);

    useEffect(() => {
        if (asyncError) {
            setShowError(true);
        }
    }, [asyncError]);

    const validation = useFormValidation({
        userName: useValidation(userName, [
            minMaxValidator(
                i18next.t('microeat.register.data.validation.error.login'),
                6
            ),
        ]),

        password: useValidation(password, [
            minMaxValidator(
                i18next.t('microeat.register.data.validation.error.password'),
                6
            ),
            requireSpesialSymbols(),
        ]),

        email: useValidation(email, [emailValidator()]),

        twoSamePasswords: useValidation(
            [password, passwordRepeat],
            [
                twoSameValue(
                    i18next.t(
                        'microeat.register.data.validation.error.twosamepasswords'
                    )
                ),
            ]
        ),
    });

    function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        if (!password || !userName || !passwordRepeat || !email) {
            setValidationError(
                i18next.t('microeat.register.data.validation.error')
            );
            firstInputRef.current.focus();
            setShowError(true);
            return;
        } else if (validation.isFormValid) {
            submitFormData(userName, password, email);
        } else {
            setShowError(true);
            return;
        }
    }

    function handleChangeUserName(event) {
        setShowError(false);
        setUserName(event.target.value);
    }

    function handleChangePassword(event) {
        setShowError(false);
        setPassword(event.target.value);
    }

    function handleChangePasswordRepeat(event) {
        setShowError(false);
        setPasswordRepeat(event.target.value);
    }

    function handleChangeEmail(event) {
        setShowError(false);
        setEmail(event.target.value);
    }

    return (
        <form className={style.loginForm} onSubmit={handleSubmit}>
            <h2 className={style.loginTitleSpacing}>
                {i18next.t('microeat.form.microeat')}
            </h2>
            <div className={style.loginInner}>
                <h3 className={style.loginTitle}>
                    {i18next.t('microeat.register.header')}
                </h3>
                <LabeledInput
                    inputRef={firstInputRef}
                    text={i18next.t(
                        'microeat.register.data.input.userName.text'
                    )}
                    id="login-input"
                    name="login"
                    value={userName}
                    onChange={handleChangeUserName}
                    error={
                        showError &&
                        (!!asyncError ||
                            !!validationError ||
                            validation.errors.userName)
                    }
                />
                <LabeledInput
                    text={i18next.t(
                        'microeat.register.data.input.password.text'
                    )}
                    id="password-input"
                    name="password"
                    type="password"
                    value={password}
                    onChange={handleChangePassword}
                    error={
                        showError &&
                        (!!asyncError ||
                            !!validationError ||
                            validation.errors.password ||
                            !!validation.errors.twoSamePasswords)
                    }
                />
                <LabeledInput
                    text={i18next.t(
                        'microeat.register.data.input.password.repeat.text'
                    )}
                    id="password-repeat-input"
                    name="password-repeat"
                    type="password"
                    value={passwordRepeat}
                    onChange={handleChangePasswordRepeat}
                    error={
                        showError &&
                        Boolean(
                            !!asyncError ||
                                !!validationError ||
                                validation.errors.twoSamePasswords
                        )
                    }
                />
                <LabeledInput
                    text={i18next.t('microeat.register.data.input.email.text')}
                    id="email-input"
                    name="email"
                    value={email}
                    onChange={handleChangeEmail}
                    error={
                        showError &&
                        (asyncError ||
                            validationError ||
                            validation.errors.email)
                    }
                />
                <h3 className={style.textRecovery}>
                    {i18next.t('microeat.register.data.email.text')}
                </h3>
                <div className={style.button}>
                    <Button type="submit" colorScheme={ButtonColors.green}>
                        {i18next.t('microeat.register.data.button.submit')}
                    </Button>
                </div>
                <div className={style.cancel}>
                    <Link href={baseUrl + URLs.login.url}>
                        {i18next.t('microeat.register.data.button.cancel')}
                    </Link>
                </div>
            </div>
        </form>
    );
}

const mapStateToProps = (state): MapStateToProps => ({
    loading: state.register.inputData.loading,
    data: state.register.inputData.data,
    asyncError: state.register.inputData.error,
    validationError: state.register.inputData.validationError,
    ...state.register.inputData.form,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
    submitFormData: (userName, password, email) =>
        dispatch(submitRegisterFormData({ userName, password, email })),
    setValidationError: (error) => dispatch(setValidationErrorAction(error)),
    setUserName: (value) => dispatch(formUserNameChangeAction(value)),
    setPassword: (value) => dispatch(formPasswordChangeAction(value)),
    setPasswordRepeat: (value) =>
        dispatch(formPasswordRepeatChangeAction(value)),
    setEmail: (value) => dispatch(formEmailChangeAction(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InputData);
