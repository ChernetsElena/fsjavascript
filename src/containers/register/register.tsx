import React, { useState } from 'react';
import style from './style.css';
import { ErrorBoundary } from '../../components';

import { RegisterStep, StageProps } from './steps/model';
import InputData from './steps/input-data';
import InputCode from './steps/input-code';

const steps = {
    [RegisterStep.DATA_STEP]: InputData,
    [RegisterStep.CODE_STEP]: InputCode,
};

function Register() {
    const [currentStep, setCurrentStep] = useState(RegisterStep.DATA_STEP);

    const Stage: React.FC<StageProps> = steps[currentStep];

    return (
        <ErrorBoundary>
            <div className={style.wrapper}>
                <Stage setStep={setCurrentStep} />
            </div>
        </ErrorBoundary>
    );
}

export default Register;
