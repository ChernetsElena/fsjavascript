﻿import React, { useState } from 'react';
import { mount } from 'enzyme';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { store } from '../../../__data__/store';
import { RegisterStep, StageProps } from '../steps/model';
import mockAdapter from 'axios-mock-adapter';
import InputCode from '../steps/input-code';
import axios from 'axios';
import InputData from '../steps/input-data';

import { describe, it, expect, beforeEach } from '@jest/globals';

const steps = {
    [RegisterStep.DATA_STEP]: InputData,
    [RegisterStep.CODE_STEP]: InputCode,
};

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift();

            if (config.url.includes(url)) {
                return response;
            }
        });
    });
};

describe('Тестирование контейнера Register', () => {
    let mock;
    beforeEach(() => {
        mock = new mockAdapter(axios);
    });
    it('Тест рендер InputData', async () => {
        const setCurrentStep = () => null;
        expect.assertions(6);
        //первый рендер 1
        const component = mount(
            <Provider store={store}>
                <InputData setStep={setCurrentStep} />
            </Provider>
        );
        expect(component).toMatchSnapshot();

        //проверка валидации на пустые поля 2
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //проверка валидации пароля, логина и почты 3
        component.find('input#login-input').simulate('change', {
            target: {
                value: 'test login',
            },
        });
        component.find('input#password-input').simulate('change', {
            target: {
                value: 'test password',
            },
        });
        component.find('input#password-repeat-input').simulate('change', {
            target: {
                value: 'test кузуфе password',
            },
        });
        component.find('input#email-input').simulate('change', {
            target: {
                value: 'test',
            },
        });
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //пользователь вводит логин, пароль, повтор пароля и почту 4
        component.find('input#login-input').simulate('change', {
            target: {
                value: 'testlogin',
            },
        });
        component.find('input#password-input').simulate('change', {
            target: {
                value: 'testpassword&',
            },
        });
        component.find('input#password-repeat-input').simulate('change', {
            target: {
                value: 'testpassword&',
            },
        });
        component.find('input#email-input').simulate('change', {
            target: {
                value: 'chernets-elena@mail.ru',
            },
        });
        component.update();
        expect(component).toMatchSnapshot();

        //регистрация с заполненными полями 5
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        const response = [
            [
                'POST',
                '/register/data',
                {},
                200,
                {
                    token: 'test token',
                },
            ],
        ];

        //перехват запроса 6
        await multipleRequest(mock, response);
        component.update();
        expect(component).toMatchSnapshot();
    });
});
