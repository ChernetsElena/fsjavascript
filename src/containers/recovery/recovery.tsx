import React, { useState } from 'react';
import style from './style.css';
import { ErrorBoundary } from '../../components';
import { RecoverPasswordStep, StageProps } from './steps/model';

import InputEmail from './steps/input-email';
import InputCode from './steps/input-code';
import InputPassword from './steps/input-password';

const steps = {
    [RecoverPasswordStep.EMAIL_STEP]: InputEmail,
    [RecoverPasswordStep.CODE_STEP]: InputCode,
    [RecoverPasswordStep.PASSWORD_STEP]: InputPassword,
};

function Recovery() {
    const [currentStep, setCurrentStep] = useState(
        RecoverPasswordStep.EMAIL_STEP
    );

    const Stage: React.FC<StageProps> = steps[currentStep];

    return (
        <ErrorBoundary>
            <div className={style.wrapper}>
                <Stage setStep={setCurrentStep} />
            </div>
        </ErrorBoundary>
    );
}

export default Recovery;
