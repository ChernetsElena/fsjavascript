import React, { useEffect, useRef, useState } from 'react';
import style from './style.css';
import { LabeledInput, Button, ButtonColors, Link } from '../../../components';
import { URLs, baseUrl } from '../../../__data__/urls';
import { RecoverPasswordStep, StageProps } from './model';
import {
    submitRecoveryFormCode,
    formCodeChange as formCodeChangeAction,
    setValidationError as setValidationErrorAction,
} from '../../../__data__/actions/recovery-code';
import { connect } from 'react-redux';
import i18next from 'i18next';

type MapStateToProps = {
    loading: boolean;
    data: any;
    asyncError: boolean | string;
    validationError: boolean | string;
    code: string;
};

type MapDispatchToProps = {
    submitFormCode: (code: string) => void;
    setCode: (value: string) => void;
    setValidationError: (error: string) => void;
};

type InputCodeProps = StageProps & MapStateToProps & MapDispatchToProps;

function InputCode({
    setStep,
    data,
    code,
    submitFormCode,
    setCode,
    validationError,
    setValidationError,
    asyncError,
}: React.PropsWithChildren<InputCodeProps>) {
    const firstInputRef = useRef(null);
    const [showError, setShowError] = useState(false);

    useEffect(() => {
        firstInputRef.current.focus();
    }, []);

    useEffect(() => {
        if (data) {
            setStep(RecoverPasswordStep.PASSWORD_STEP);
        }
    }, [data]);

    useEffect(() => {
        if (asyncError) {
            setShowError(true);
        }
    }, [asyncError]);

    async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        if (!code) {
            setValidationError(
                i18next.t('microeat.recovery.input.code.validation.error')
            );
            firstInputRef.current.focus();
            setShowError(true);
            return;
        }
        submitFormCode(code);
    }

    function handleChangeCode(event) {
        setCode(event.target.value);
        setShowError(false);
    }

    return (
        <form className={style.loginForm} onSubmit={handleSubmit}>
            <h2 className={style.loginTitleSpacing}>
                {i18next.t('microeat.form.microeat')}
            </h2>
            <div className={style.loginInner}>
                <h3 className={style.loginTitle}>
                    {i18next.t('microeat.recovery.header')}
                </h3>
                <LabeledInput
                    inputRef={firstInputRef}
                    text={i18next.t('microeat.recovery.code.input.text')}
                    id="code-input"
                    name="code"
                    value={code}
                    onChange={handleChangeCode}
                    error={showError && (asyncError || validationError)}
                />
                <div className={style.button}>
                    <Button
                        type="submit"
                        colorScheme={ButtonColors.green}
                        className={style.bigButton}
                    >
                        {i18next.t('microeat.recovery.code.button.submit')}
                    </Button>
                </div>
                <div className={style.cancel}>
                    <Link href={baseUrl + URLs.login.url}>
                        {i18next.t('microeat.recovery.code.button.cancel')}
                    </Link>
                </div>
            </div>
        </form>
    );
}

const mapStateToProps = (state): MapStateToProps => ({
    loading: state.recovery.inputCode.loading,
    data: state.recovery.inputCode.data,
    asyncError: state.recovery.inputCode.error,
    validationError: state.recovery.inputCode.validationError,
    ...state.recovery.inputCode.form,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
    submitFormCode: (code) => dispatch(submitRecoveryFormCode({ code })),
    setValidationError: (error) => dispatch(setValidationErrorAction(error)),
    setCode: (value) => dispatch(formCodeChangeAction(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InputCode);
