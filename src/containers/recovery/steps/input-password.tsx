import React, { useEffect, useRef, useState } from 'react';
import style from './style.css';
import { Redirect } from 'react-router-dom';
import { LabeledInput, Button, ButtonColors, Link } from '../../../components';
import { StageProps } from './model';
import { URLs, baseUrl } from '../../../__data__/urls';
import {
    submitRecoveryFormPassword,
    formPasswordChange as formPasswordChangeAction,
    formPasswordRepeatChange as formPasswordRepeatChangeAction,
    setValidationError as setValidationErrorAction,
} from '../../../__data__/actions/recovery-password';
import { connect } from 'react-redux';
import {
    minMaxValidator,
    useValidation,
    requireSpesialSymbols,
    twoSameValue,
    useFormValidation,
} from '../../../utils';
import i18next from 'i18next';

type MapStateToProps = {
    loading: boolean;
    data: any;
    asyncError: boolean | string;
    validationError: boolean | string;
    password: string;
    passwordRepeat: string;
};

type MapDispatchToProps = {
    submitFormPassword: (code: string) => void;
    setPassword: (value: string) => void;
    setValidationError: (error: string) => void;
    setPasswordRepeat: (value: string) => void;
};

type InputPasswordProps = StageProps & MapStateToProps & MapDispatchToProps;

function InputPassword({
    data,
    password,
    passwordRepeat,
    submitFormPassword,
    setPassword,
    setPasswordRepeat,
    validationError,
    setValidationError,
    asyncError,
}: React.PropsWithChildren<InputPasswordProps>) {
    const firstInputRef = useRef(null);
    const [needRedirect, setNeedRedirect] = useState(false);
    const [showError, setShowError] = useState(false);

    useEffect(() => {
        firstInputRef.current.focus();
    }, []);

    useEffect(() => {
        if (data) {
            setNeedRedirect(true);
        }
    }, [data]);

    const validation = useFormValidation({
        password: useValidation(password, [
            minMaxValidator(
                i18next.t(
                    'microeat.recovery.password.validation.error.password'
                ),
                6
            ),
            requireSpesialSymbols(),
        ]),

        twoSamePasswords: useValidation(
            [password, passwordRepeat],
            [
                twoSameValue(
                    i18next.t(
                        'microeat.recovery.password.validation.error.twosamepasswords'
                    )
                ),
            ]
        ),
    });

    async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        if (!password || !passwordRepeat) {
            setValidationError(
                i18next.t('microeat.recovery.password.validation.error')
            );
            firstInputRef.current.focus();
            setShowError(true);
            return;
        } else if (validation.isFormValid) {
            submitFormPassword(password);
        } else {
            setShowError(true);
            return;
        }
    }

    function handleChangePassword(event) {
        setShowError(false);
        setPassword(event.target.value);
    }

    function handleChangePasswordRepeat(event) {
        setShowError(false);
        setPasswordRepeat(event.target.value);
    }

    if (needRedirect) {
        return <Redirect to={URLs.login.url} />;
    }

    return (
        <form className={style.loginForm} onSubmit={handleSubmit}>
            <h2 className={style.loginTitleSpacing}>
                {i18next.t('microeat.form.microeat')}
            </h2>
            <div className={style.loginInner}>
                <h3 className={style.loginTitle}>
                    {i18next.t('microeat.recovery.header')}
                </h3>
                <LabeledInput
                    inputRef={firstInputRef}
                    text={i18next.t('microeat.recovery.password.input.text')}
                    id="password-input"
                    name="password"
                    value={password}
                    type="password"
                    onChange={handleChangePassword}
                    error={
                        showError &&
                        (Boolean(asyncError) ||
                            Boolean(validationError) ||
                            Boolean(validation.errors.password) ||
                            Boolean(validation.errors.twoSamePasswords))
                    }
                />
                <LabeledInput
                    text={i18next.t(
                        'microeat.recovery.password.input.repeat.text'
                    )}
                    id="password-repeat-input"
                    name="password-repeat"
                    value={passwordRepeat}
                    type="password"
                    onChange={handleChangePasswordRepeat}
                    error={
                        showError &&
                        (asyncError ||
                            validationError ||
                            validation.errors.password ||
                            validation.errors.twoSamePasswords)
                    }
                />
                <div className={style.button}>
                    <Button
                        type="submit"
                        colorScheme={ButtonColors.green}
                        className={style.bigButton}
                    >
                        {i18next.t('microeat.recovery.password.button.submit')}
                    </Button>
                </div>
                <div className={style.cancel}>
                    <Link href={baseUrl + URLs.login.url}>
                        {i18next.t('microeat.recovery.password.button.cancel')}
                    </Link>
                </div>
            </div>
        </form>
    );
}

const mapStateToProps = (state): MapStateToProps => ({
    loading: state.recovery.inputPassword.loading,
    data: state.recovery.inputPassword.data,
    asyncError: state.recovery.inputPassword.error,
    validationError: state.recovery.inputPassword.validationError,
    ...state.recovery.inputPassword.form,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
    submitFormPassword: (password) =>
        dispatch(submitRecoveryFormPassword({ password })),
    setValidationError: (error) => dispatch(setValidationErrorAction(error)),
    setPassword: (value) => dispatch(formPasswordChangeAction(value)),
    setPasswordRepeat: (value) =>
        dispatch(formPasswordRepeatChangeAction(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InputPassword);
