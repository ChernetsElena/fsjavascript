import React, { useEffect, useRef, useState } from 'react';
import style from './style.css';
import { LabeledInput, Button, ButtonColors, Link } from '../../../components';
import { URLs, baseUrl } from '../../../__data__/urls';
import { RecoverPasswordStep, StageProps } from './model';
import {
    useValidation,
    useFormValidation,
    emailValidator,
} from '../../../utils';
import i18next from 'i18next';
import {
    submitRecoveryFormEmail,
    formEmailChange as formEmailChangeAction,
    setValidationError as setValidationErrorAction,
} from '../../../__data__/actions/recovery-email';
import { connect } from 'react-redux';

type MapStateToProps = {
    loading: boolean;
    data: any;
    asyncError: boolean | string;
    validationError: boolean | string;
    email: string;
};

type MapDispatchToProps = {
    submitFormEmail: (email: string) => void;
    setEmail: (value: string) => void;
    setValidationError: (error: string) => void;
};

type InputEmailProps = StageProps & MapStateToProps & MapDispatchToProps;

function InputEmail({
    setStep,
    data,
    email,
    submitFormEmail,
    setEmail,
    validationError,
    setValidationError,
    asyncError,
}: React.PropsWithChildren<InputEmailProps>) {
    const firstInputRef = useRef(null);
    const [showError, setShowError] = useState(false);

    useEffect(() => {
        firstInputRef.current.focus();
    }, []);

    useEffect(() => {
        if (data) {
            setStep(RecoverPasswordStep.CODE_STEP);
        }
    }, [data]);

    useEffect(() => {
        if (asyncError) {
            setShowError(true);
        }
    }, [asyncError]);

    const validation = useFormValidation({
        email: useValidation(email, [emailValidator()]),
    });

    async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        if (!email) {
            setValidationError(
                i18next.t('microeat.recovery.input.email.validation.error')
            );
            firstInputRef.current.focus();
            setShowError(true);
            return;
        } else if (validation.isFormValid) {
            submitFormEmail(email);
        } else {
            setShowError(true);
            return;
        }
    }

    function handleChangeEmail(event) {
        setShowError(false);
        setEmail(event.target.value);
    }

    return (
        <form className={style.loginForm} onSubmit={handleSubmit}>
            <h2 className={style.loginTitleSpacing}>
                {i18next.t('microeat.form.microeat')}
            </h2>
            <div className={style.loginInner}>
                <h3 className={style.loginTitle}>
                    {i18next.t('microeat.recovery.header')}
                </h3>
                <LabeledInput
                    inputRef={firstInputRef}
                    text={i18next.t('microeat.recovery.email.input.text')}
                    id="email-input"
                    name="email"
                    value={email}
                    onChange={handleChangeEmail}
                    error={
                        showError &&
                        (asyncError ||
                            validationError ||
                            validation.errors.email)
                    }
                />
                <h3 className={style.textRecovery}>
                    {i18next.t('microeat.recovery.email.text')}
                </h3>
                <div className={style.button}>
                    <Button
                        type="submit"
                        colorScheme={ButtonColors.green}
                        className={style.bigButton}
                    >
                        {i18next.t('microeat.recovery.email.button.submit')}
                    </Button>
                </div>
                <div className={style.cancel}>
                    <Link href={baseUrl + URLs.login.url}>
                        {i18next.t('microeat.recovery.email.button.cancel')}
                    </Link>
                </div>
            </div>
        </form>
    );
}

const mapStateToProps = (state): MapStateToProps => ({
    loading: state.recovery.inputEmail.loading,
    data: state.recovery.inputEmail.data,
    asyncError: state.recovery.inputEmail.error,
    validationError: state.recovery.inputEmail.validationError,
    ...state.recovery.inputEmail.form,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
    submitFormEmail: (email) => dispatch(submitRecoveryFormEmail({ email })),
    setValidationError: (error) => dispatch(setValidationErrorAction(error)),
    setEmail: (value) => dispatch(formEmailChangeAction(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InputEmail);
