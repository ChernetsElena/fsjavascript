export enum RecoverPasswordStep {
    EMAIL_STEP,
    CODE_STEP,
    PASSWORD_STEP,
}

export type StageProps = {
    setStep: (step: RecoverPasswordStep) => void;
};
