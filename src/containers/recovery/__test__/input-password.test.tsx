﻿import React from 'react';
import { mount } from 'enzyme';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { store } from '../../../__data__/store';
import { RecoverPasswordStep, StageProps } from '../steps/model';
import axios from 'axios';
import mockAdapter from 'axios-mock-adapter';

import InputEmail from '../steps/input-email';
import InputCode from '../steps/input-code';
import InputPassword from '../steps/input-password';

import { describe, it, expect, beforeEach } from '@jest/globals';

const steps = {
    [RecoverPasswordStep.CODE_STEP]: InputCode,
    [RecoverPasswordStep.EMAIL_STEP]: InputEmail,
    [RecoverPasswordStep.PASSWORD_STEP]: InputPassword,
};

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift();

            if (config.url.includes(url)) {
                return response;
            }
        });
    });
};

describe('Тестирование контейнера Recovery', () => {
    let mock;
    beforeEach(() => {
        mock = new mockAdapter(axios);
    });
    it('Тест рендер InputPassword', async () => {
        const setCurrentStep = () => null;
        expect.assertions(6);
        //первый рендер
        const component = mount(
            <Provider store={store}>
                <InputPassword setStep={setCurrentStep} />)
            </Provider>
        );
        expect(component).toMatchSnapshot();

        //проверка валидации на пустые поля
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //проверка валидации паролей
        component.find('input#password-input').simulate('change', {
            target: {
                value: 'testpassword',
            },
        });
        component.find('input#password-repeat-input').simulate('change', {
            target: {
                value: 'test',
            },
        });
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //пользователь вводит пароль
        component.find('input#password-input').simulate('change', {
            target: {
                value: 'testpassword&',
            },
        });
        component.find('input#password-repeat-input').simulate('change', {
            target: {
                value: 'testpassword&',
            },
        });
        component.update();
        expect(component).toMatchSnapshot();

        //input-password с заполненными полями
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        const response = [
            [
                'POST',
                '/recovery/password',
                {},
                200,
                {
                    token: 'test token',
                },
            ],
        ];

        //перехват запроса
        await multipleRequest(mock, response);
        component.update();
        expect(component).toMatchSnapshot();
    });
});
