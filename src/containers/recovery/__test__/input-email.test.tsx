﻿import React from 'react';
import { mount } from 'enzyme';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { store } from '../../../__data__/store';
import mockAdapter from 'axios-mock-adapter';
import { RecoverPasswordStep, StageProps } from '../steps/model';
import InputEmail from '../steps/input-email';
import InputCode from '../steps/input-code';
import InputPassword from '../steps/input-password';
import axios from 'axios';

import { describe, it, expect, beforeEach } from '@jest/globals';

const steps = {
    [RecoverPasswordStep.CODE_STEP]: InputCode,
    [RecoverPasswordStep.EMAIL_STEP]: InputEmail,
    [RecoverPasswordStep.PASSWORD_STEP]: InputPassword,
};

const multipleRequest = async (mock, responses) => {
    await act(async () => {
        await mock.onAny().reply((config) => {
            const [method, url, params, ...response] = responses.shift();

            if (config.url.includes(url)) {
                return response;
            }
        });
    });
};

describe('Тестирование контейнера Recovery', () => {
    let mock;
    beforeEach(() => {
        mock = new mockAdapter(axios);
    });
    it('Тест рендер InputEmail', async () => {
        const setCurrentStep = () => null;
        expect.assertions(6);
        //первый рендер
        const component = mount(
            <Provider store={store}>
                <InputEmail setStep={setCurrentStep} />)
            </Provider>
        );
        expect(component).toMatchSnapshot();

        //проверка валидации на пустые поля
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //проверка валидации почты
        component.find('input#email-input').simulate('change', {
            target: {
                value: 'test',
            },
        });
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        //пользователь вводит почту
        component.find('input#email-input').simulate('change', {
            target: {
                value: 'chernets-elena@mail.ru',
            },
        });
        component.update();
        expect(component).toMatchSnapshot();

        //input-email с заполненными полями
        component.find('form').simulate('submit');
        component.update();
        expect(component).toMatchSnapshot();

        const response = [
            [
                'POST',
                '/recovery/email',
                {},
                200,
                {
                    token: 'test token',
                },
            ],
        ];

        //перехват запроса
        await multipleRequest(mock, response);
        component.update();
        expect(component).toMatchSnapshot();
    });
});
