import React from 'react';
import ReactDom from 'react-dom';
import i18next from 'i18next';
import { i18nextInitConfig } from '@ijl/cli';
import App from './app';

i18next.t = i18next.t.bind(i18next);

const i18nextPromise = i18nextInitConfig(i18next);

export default () => <App />;

export const mount = async (Сomponent) => {
    await Promise.all([i18nextPromise]);
    ReactDom.render(<Сomponent />, document.getElementById('app'));
};

export const unmount = () => {
    ReactDom.unmountComponentAtNode(document.getElementById('app'));
};
