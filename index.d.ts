﻿declare module '*.css' {
    interface Style {
        [key: string]: string;
    }

    const style: Style;

    export default style;
}

declare module '*.jpg' {
    const value: string;

    export default value;
}

declare module '*.json' {
    const value: any;

    export default value;
}
